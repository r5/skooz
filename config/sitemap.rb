SitemapGenerator::Sitemap.create_index = true
SitemapGenerator::Sitemap.default_host = "#{WEBSITE_NAME}"

SitemapGenerator::Sitemap.create do
    group(:sitemaps_path => "sitemap/", :filename => :pages) do
    Page.where(:published => true, :spam => false).find_each do |page|
      add page.slug_path, :lastmod => 2.days.ago, :changefreq => 'monthly', :priority => 0.9
    end
  end

  group(:sitemaps_path => "sitemap/", :filename => :tags) do
    ActsAsTaggableOn::Tag.find_each do |tag|
       add "/tag/#{tag.name}", :lastmod => 2.days.ago, :changefreq => 'weekly', :priority => 0.9
    end
  end

  group(:sitemaps_path => "sitemap/", :filename => :interests) do
    Interest.active.find_each do |interest|
      add "interest/#{interest.slug}", :lastmod => 2.days.ago, :changefreq => 'weekly', :priority => 0.9
    end
  end

  group(:sitemaps_path => "sitemap/", :filename => :users) do
    User.where(:spam => false).find_each do |u|
      add "/users/#{u.slug}/likes", :lastmod => 2.days.ago, :changefreq => 'weekly', :priority => 0.9
    end
  end
end
