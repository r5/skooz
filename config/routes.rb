Sqooz::Application.routes.draw do

  

  
  resources :request_invites, :only => [:new, :create]
  # use_link_thumbnailer

  get "paid/sqoozers" => "static#paid_sqoozers", :as => :paid_sqoozer
  match "samples/email" => "static#send_sample_email", :as => :send_sample_email, via: [:get, :post]
  post "suggest_interest" => "interests#suggest", :as => :suggest_interest
  get "recommended(/:page)" => "feed#recommended", :as => :feed, constraints: { page: /\d+/ }

  get "search/(:page)" => "pages#search", :as => :search

  get "feed/activity(/:page)" => "feed#activity", :as => :feed_activity, constraints: { page: /\d+/ }
  get "trending(/:page)" => "feed#trending", :as => :feed_trending, constraints: { page: /\d+/ }
  get "feed/lists(/:page)" => "feed#lists", :as => :feed_list, constraints: { page: /\d+/ }
  get "feed/channels(/:page)" => "feed#channels", :as => :feed_channels, constraints: { page: /\d+/ }
  get "social/page/:id" => "pages#share_page", :as => :share_page
  devise_for :users

  resources :interests, :only => [:create, :destroy, :index]
  get "interest/:id/(/:page)"  => "interests#show", :as => :show_interest, constraints: { page: /\d+/ }
  get "tag/:name/(/:page)"  => "tags#show", :as => :show_tag, constraints: { page: /\d+/ }
  get "tags/(/:page)"  => "tags#index", :as => :tags, constraints: { page: /\d+/ }
  get "top/sqoozers(/:page)" => "discover#top_users", :as => :top_users, constraints: { page: /\d+/ }
  get "top/channels(/:page)" => "discover#top_channels", :as => :top_channels, constraints: { page: /\d+/ }
  resources :users do
    member do
      get "following/lists(/:page)"  => "users#following_lists", :as => :following_lists, constraints: { page: /\d+/ }
      get "following/channels(/:page)"  => "users#following_channels", :as => :following_channels, constraints: { page: /\d+/ }
      get "following(/:page)"  => "users#following", :as => :following, constraints: { page: /\d+/ }
     
      get "followers(/:page)"  => "users#followers", :as => :followers, constraints: { page: /\d+/ }
      get "interests(/:page)"  => "users#interests", :as => :interests, constraints: { page: /\d+/ }
      get "history(/:page)"  => "users#history", :as => :history, constraints: { page: /\d+/ }
      get "lists(/:page)"  => "users#lists", :as => :lists, constraints: { page: /\d+/ }
      get "comments(/:page)"  => "users#comments", :as => :comments, constraints: { page: /\d+/ }
      get "channels(/:page)"  => "users#channels", :as => :channels, constraints: { page: /\d+/ }
      get "notifications(/:page)"  => "users#notifications", :as => :notifications, constraints: { page: /\d+/ }
      get "likes(/:page)"  => "users#likes", :as => :likes,  constraints: {page: /\d+/}
      get "likes/filter/:filter(/:page)" => "users#likes", :as => :likes_ptype, constraints: { page: /\d+/ }
      get "likes/interest/:iname(/:page)" => "users#likes", :as => :likes_interest, constraints: { page: /\d+/ }
       get "likes/channel/:cname(/:page)" => "users#likes", :as => :likes_channel, constraints: { page: /\d+/ }
      get "pages(/:page)"  => "users#pages", :as => :pages,  constraints: {page: /\d+/}
      get "pages/list/:filter(/:page)" => "users#pages", :as => :pages_ptype, constraints: { page: /\d+/ }
      get "pages/interest/:iname(/:page)" => "users#pages", :as => :pages_interest, constraints: { page: /\d+/ }
      get "pages/channel/:cname(/:page)" => "users#pages", :as => :pages_channel, constraints: { page: /\d+/ }
      # get "pages/tags/:tname(/:page)" => "users#pages", :as => :pages_tag, constraints: { page: /\d+/ }
      # get "tags"
    end

    collection do
      post "report_page" => "users#report", :as => :report
    end
  end

  post "follow" => "users#follow", :as => :follow

  resources :channels, :only => [:new, :edit,:create, :index, :show]
  resources :listings, :only => [:create, :destroy, :index]
  resources :comments, :only => [:create, :destroy, :index]
  resources :lists, :only => [:create, :destroy, :index]
  resources :likes, :only => [:create, :destroy]
  resources :pages, :only => [:create, :new, :edit, :update, :destroy] do
    member do
      get "about"
      post "about"
    end
    collection do
      post "photo/upload" => "pages#medium_photo_upload"
      post "photo/delete" => "pages#medium_photo_delete"
      # post "getthumb"
    end
  end

  post "pages/sqooz" => "pages#sqoozme", :as => :sqoozit
  get "sqoozing" => "pages#sqoozme", :as => :sqoozit_ad
  get "video/:sitename/:id" => "pages#show", :as => :video_show, :constraints => { :sitename => /[^\/]*/, :id => /[^\/]*/ }
  get "page/:id" => "pages#show", :as => :page_show, :constraints => { :sitename => /[^\/]*/, :id => /[^\/]*/ }
  get "about" => "static#about"
  get "help" => "static#help"
  get "discover/interests(/:page)" => "discover#interests", :as => :discover_interests
  # get "discover/sqoozers(/:page)" => "discover#sqoozers", :as => :discover_sqoozers
  # get "discover/lists(/:page)" => "discover#lists", :as => :discover_lists
  get "discover/channels(/:page)" => "discover#channels", :as => :discover_channels
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # authenticated :user do
  #   root :to => 'users#show'
  # end
  root :to => 'static#welcome'
  # root :to => "static#coming_soon"
  # root :to => 'users#show', :constraints => lambda{ |req| req.session['warden.user.user.key'][0] == 'User' }


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
