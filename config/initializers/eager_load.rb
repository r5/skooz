class ActiveRecord::Base
  def self.eagerLoadFor(records, associations)
    ActiveRecord::Associations::Preloader.new(records, associations).run
  end
end