class FulltextsearchPages < ActiveRecord::Migration
  def up
    execute 'alter table pages engine myisam'
    execute "CREATE FULLTEXT INDEX fulltext_pages_title ON pages (title)"
    execute "CREATE FULLTEXT INDEX fulltext_pages_about ON pages (about)"
    execute "CREATE FULLTEXT INDEX fulltext_search_pages ON pages (title, about)"
  end

  def down
     # execute "ALTER TABLE pages DROP INDEX fulltext_seach_pages"
  end
end
