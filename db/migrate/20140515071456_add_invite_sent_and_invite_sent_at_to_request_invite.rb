class AddInviteSentAndInviteSentAtToRequestInvite < ActiveRecord::Migration
  def change
    add_column :request_invites, :invite_sent, :boolean
    add_column :request_invites, :and_invite_sent_at, :datetime
  end
end
