class AddLastPaidAtToUser < ActiveRecord::Migration
  def change
    add_column :users, :last_paid_at, :datetime
  end
end
