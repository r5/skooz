class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.integer :user_id
      t.integer :page_id
      t.boolean :spam
      t.boolean :liked
      t.integer :interest_id

      t.timestamps
    end
    add_index :likes, :page_id
    add_index :likes, [:user_id, :page_id], :unique => true
    add_index :likes, [:user_id, :interest_id]
  end
end
