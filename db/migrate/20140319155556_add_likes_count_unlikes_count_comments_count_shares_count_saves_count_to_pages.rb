class AddLikesCountUnlikesCountCommentsCountSharesCountSavesCountToPages < ActiveRecord::Migration

  def self.up

    add_column :pages, :likes_count, :integer, :null => false, :default => 0

    add_column :pages, :unlikes_count, :integer, :null => false, :default => 0

    add_column :pages, :comments_count, :integer, :null => false, :default => 0

    add_column :pages, :shares_count, :integer, :null => false, :default => 0

    add_column :pages, :saves_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :pages, :likes_count

    remove_column :pages, :unlikes_count

    remove_column :pages, :comments_count

    remove_column :pages, :shares_count

    remove_column :pages, :saves_count

  end

end
