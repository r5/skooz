class CreateInterestings < ActiveRecord::Migration
  def change
    create_table :interestings do |t|
      t.integer :user_id
      t.integer :interest_id

      t.timestamps
    end
    # add_index :interestings, :user_id    # It is not required as we have to following, TODO: ENsure this
    # add_index :interestings, :interest_id
    add_index :interestings, [:user_id, :interest_id], :unique => true
  end
end
