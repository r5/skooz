class UpdatePagesFulltextSearchIndex < ActiveRecord::Migration
  def up
    execute "ALTER TABLE pages DROP INDEX fulltext_pages_title"
    execute "ALTER TABLE pages DROP INDEX fulltext_pages_about"
    execute "ALTER TABLE pages DROP INDEX fulltext_search_pages"
    execute "CREATE FULLTEXT INDEX fulltext_pages_title ON pages (title)"
    execute "CREATE FULLTEXT INDEX fulltext_pages_about ON pages (plain_about)"
    execute "CREATE FULLTEXT INDEX fulltext_search_pages ON pages (title, plain_about)"
  end

  def down
    execute "ALTER TABLE pages DROP INDEX fulltext_pages_title"
    execute "ALTER TABLE pages DROP INDEX fulltext_pages_about"
    execute "ALTER TABLE pages DROP INDEX fulltext_seach_pages"
  end
end
