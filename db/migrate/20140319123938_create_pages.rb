class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.integer :user_id
      t.string :url
      t.integer :ptype
      t.boolean :published, :default => true
      t.boolean :featured, :default => false
      t.boolean :spam, :default => false
      t.string :title
      t.text :about
      t.string :slug
      t.string :source_title
      t.string :source_description
      t.integer :interest_id

      t.timestamps
    end
    add_index :pages, :slug, unique: true
    add_index :pages, :user_id
  end
end
