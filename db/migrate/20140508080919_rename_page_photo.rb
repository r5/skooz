class RenamePagePhoto < ActiveRecord::Migration
  def self.up
    rename_column :pages, :photo_file_name, :cover_photo_file_name
    rename_column :pages, :photo_file_size, :cover_photo_file_size
    rename_column :pages, :photo_content_type, :cover_photo_content_type
    rename_column :pages, :photo_updated_at, :cover_photo_updated_at       
  end

  def self.down
    # rename back if you need or do something else or do nothing
  end
end
