class RenameColumnInviteSentAt < ActiveRecord::Migration
  def change
    rename_column :request_invites, :and_invite_sent_at, :invite_sent_at
  end
end
