class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :resource_id
      t.string :resource_type
      t.integer :user_id
      t.boolean :closed, :default => false
      t.boolean :viewed, :default => false
      t.integer :actor_id
      t.integer :page_id
      t.integer :created_at_int
      t.timestamps
    end
    add_index :notifications, [:user_id , :viewed]
    add_index :notifications, :created_at_int
  end
end
