class AddAttachmentCoverPhotoToChannels < ActiveRecord::Migration
  def self.up
    change_table :channels do |t|
      t.attachment :cover_photo
    end
  end

  def self.down
    drop_attached_file :channels, :cover_photo
  end
end
