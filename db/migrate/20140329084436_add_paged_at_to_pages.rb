class AddPagedAtToPages < ActiveRecord::Migration
  def change
    add_column :pages, :paged_at, :integer
    add_index :pages, :paged_at
  end
end
