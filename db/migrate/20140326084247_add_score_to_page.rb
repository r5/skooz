class AddScoreToPage < ActiveRecord::Migration
  def change
    add_column :pages, :score, :integer, :null => false, :default => 0
    add_column :pages, :domain_approved, :boolean, :default => false
    add_column :pages, :framecode, :string
  end
end
