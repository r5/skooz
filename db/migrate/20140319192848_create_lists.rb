class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.string :title
      t.string :about
      t.integer :user_id
      t.boolean :private, :default => false

      t.timestamps
    end
    # add_index :lists, :user_id
  end
end
