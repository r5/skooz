class AddChannelIdToPages < ActiveRecord::Migration
  def change
    add_column :pages, :channel_id, :integer
    add_index :pages, :channel_id
  end
end
