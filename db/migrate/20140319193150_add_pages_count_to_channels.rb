class AddPagesCountToChannels < ActiveRecord::Migration

  def self.up

    add_column :channels, :pages_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :channels, :pages_count

  end

end
