class AddRandOrderNoToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :rand_order_no, :integer
    remove_index :pages, name: "interest_score_paged_at_id_domain"
    add_index :pages, [:rand_order_no, :score, :paged_at, :id ,:interest_id,:all_domain_id], name: "rand_order_no_score_paged_at_id_interest_domain"

  end

  def self.down
    remove_column :pages, :rand_order_no
    add_index :pages, [:interest_id, :score, :paged_at, :all_domain_id], name: "interest_score_paged_at_id_domain"
    remove_index :pages, name: "rand_order_no_score_paged_at_id_interest_domain"
   
  end
end
