class AddAttachmentPattachmentToPhotos < ActiveRecord::Migration
  def self.up
    change_table :photos do |t|
      t.attachment :pattachment
    end
  end

  def self.down
    drop_attached_file :photos, :pattachment
  end
end
