class ChangeColumnAboutChannel < ActiveRecord::Migration
  def change
    change_column :channels, :about,  :text
  end
end
