class AddChannelsCountAndListingsCountAndPagesCountAndCommentsCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :channels_count, :integer, :null => false, :default => 0
    add_column :users, :listings_count, :integer, :null => false, :default => 0
    add_column :users, :pages_count, :integer, :null => false, :default => 0
    add_column :users, :comments_count, :integer, :null => false, :default => 0
  end
end
