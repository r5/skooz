class AddListsCountLikesCountToUsers < ActiveRecord::Migration

  def self.up

    add_column :users, :lists_count, :integer, :null => false, :default => 0

    add_column :users, :likes_count, :integer, :null => false, :default => 0

    remove_column :users, :listings_count

  end

  def self.down

    remove_column :users, :lists_count

    remove_column :users, :likes_count

  end

end
