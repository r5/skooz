class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.integer :page_id
      t.integer :list_id
      t.integer :user_id
      t.timestamps
    end
    # add_index :listings, :page_id
    # add_index :listings, :list_id # It shoudl use the following index: TODO: Make sure of it
    add_index :listings, :user_id
    add_index :listings, [:page_id, :list_id], :unique => true
  end
end
