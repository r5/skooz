class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.string :title
      t.string :about
      t.integer :user_id
      t.string :ctype

      t.timestamps
    end
    add_index :channels, :user_id
  end
end
