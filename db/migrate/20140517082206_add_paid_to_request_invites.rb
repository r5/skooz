class AddPaidToRequestInvites < ActiveRecord::Migration
  def change
    add_column :request_invites, :paid, :boolean
  end
end
