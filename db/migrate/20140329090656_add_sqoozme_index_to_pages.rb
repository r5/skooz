class AddSqoozmeIndexToPages < ActiveRecord::Migration
  def change
    add_column :pages, :all_domain_id, :integer
    add_column :all_domains, :blocks_count, :integer, :null => false, :default => 0
    add_column :all_domains, :spams_count, :integer, :null => false, :default => 0
    add_column :all_domains, :pages_count, :integer, :null => false, :default => 0
    add_column :all_domains, :noloading_count, :integer, :null => false, :default => 0
    add_column :all_domains, :unlikes_count, :integer, :null => false, :default => 0
    add_column :users, :blocked_pages, :text
    add_index :pages, [:interest_id, :score, :paged_at, :id, :all_domain_id], :name => 'interest_score_paged_at_id_domain'
    # add_index :pages, [:paged_at, :score, :id, :all_domain_id], :name => 'paged_at_score_id_domain'  #TODO Thought I have removed this but make sure the above is being used everywhere
  end
end
