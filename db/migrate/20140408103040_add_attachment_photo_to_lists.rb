class AddAttachmentPhotoToLists < ActiveRecord::Migration
  def self.up
    change_table :lists do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :lists, :photo
  end
end
