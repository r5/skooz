class AddCoverPhotoUrlToLists < ActiveRecord::Migration
  def change
    add_column :lists, :cover_photo_url, :string
  end
end
