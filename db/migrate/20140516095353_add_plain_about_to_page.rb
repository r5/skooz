class AddPlainAboutToPage < ActiveRecord::Migration
  def change
    add_column :pages, :plain_about, :text
  end
end
