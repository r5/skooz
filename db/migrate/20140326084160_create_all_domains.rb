class CreateAllDomains < ActiveRecord::Migration
  def change
    create_table :all_domains do |t|
      t.string :domain
      t.integer :score , :null => false, :default => 0
      t.boolean :video
      t.string :embed
      t.integer :width
      t.integer :height
      t.boolean :spam, :default => false
      t.boolean :approved, :default => false
      t.string :provider
      t.string :id_regex
      t.timestamps
    end
    add_index :all_domains, :domain
  end
end
