class AddSpamToUsers < ActiveRecord::Migration
  def change
    add_column :users, :spam, :boolean, :default => false
  end
end
