class CreatePhotos < ActiveRecord::Migration
  def change
    #TODO: remove photo columns from page
    create_table :photos do |t|
      t.integer :page_id
      t.string :caption

      t.timestamps
    end
    add_index :photos,  [:page_id]
  end
end
