class CreateRequestInvites < ActiveRecord::Migration
  def change
    create_table :request_invites do |t|
      t.string :name
      t.text :about
      t.boolean :author, :default => false
      t.text :what_write

      t.timestamps
    end
  end
end
