class AddInterestInToListings < ActiveRecord::Migration
  def change
    add_column :listings, :interest_id, :integer
    add_index :listings, [:list_id, :interest_id]
    add_index :listings, [:user_id, :list_id]
    add_index :listings, [:user_id, :interest_id]
    add_index :listings, [:page_id,:interest_id, :user_id]
  end
end
