class FollowCountersToListsChannelsAndUsers < ActiveRecord::Migration
  def change
    add_column :lists, :followers_counter, :integer, :null => false, :default => 0
    add_column :channels, :followers_counter, :integer, :null => false, :default => 0
    add_column :users, :followers_counter, :integer, :null => false, :default => 0
    add_column :users, :follows_counter, :integer, :null => false, :default => 0
  end
end
