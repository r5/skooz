class CreatePageViews < ActiveRecord::Migration
  def up
    create_table :page_views do |t|
      t.integer :page_id
      t.integer :user_id
      t.integer :score
      t.integer :viewed_at
      t.timestamps
    end
    add_index :page_views, [:user_id ,:viewed_at]
  end

  def down
    drop_table :page_views
  end
end
