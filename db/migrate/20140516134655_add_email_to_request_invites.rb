class AddEmailToRequestInvites < ActiveRecord::Migration
  def change
    add_column :request_invites, :email, :string
  end
end
