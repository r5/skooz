class AddMaleBirthdateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :male, :boolean, :default => true
    add_column :users, :birthday, :date
  end
end
