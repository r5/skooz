class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :user_id
      t.integer :subject_id
      t.string :subject_type
      t.string :slug
      t.integer :page_id
      t.string :activity_str
      t.integer :real_subject_id

      t.timestamps
    end
    add_index :activities, :slug
    add_index :activities, :user_id
    # add_index :activities, :page_id # This is not required as it now
  end
end
