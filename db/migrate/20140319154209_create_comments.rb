class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.string :title
      t.text :content
      t.integer :page_id
      t.boolean :spam, :default => false

      t.timestamps
    end
    add_index :comments, :user_id
    add_index :comments, :page_id
  end
end
