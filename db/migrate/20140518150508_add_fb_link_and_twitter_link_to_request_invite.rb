class AddFbLinkAndTwitterLinkToRequestInvite < ActiveRecord::Migration
  def change
    add_column :request_invites, :fb_link, :string
    add_column :request_invites, :twitter_link, :string
  end
end
