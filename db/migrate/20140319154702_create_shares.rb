class CreateShares < ActiveRecord::Migration
  def change
    create_table :shares do |t|
      t.integer :user_id
      t.string :shared_via
      t.text :shared_emails
      t.string :share_entity
      t.boolean :shared
      t.integer :page_id

      t.timestamps
    end
    add_index :shares, :user_id
    add_index :shares, :page_id
  end
end
