class CreateInterests < ActiveRecord::Migration
  def change
    create_table :interests do |t|
      t.string :name
      t.boolean :active, :default => false
      t.attachment :image
      t.timestamps
    end
    add_index :interests, :name
  end
end
