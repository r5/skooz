class AddPagesCountToLists < ActiveRecord::Migration

  def self.up

    add_column :lists, :pages_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :lists, :pages_count

  end

end
