class AddInviteForJobToUsers < ActiveRecord::Migration
  def change
    add_column :users, :invite_for_job, :boolean, :default => false
  end
end
