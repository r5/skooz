class AddPagesCountToInterests < ActiveRecord::Migration

  def self.up

    add_column :interests, :pages_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :interests, :pages_count

  end

end
