# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140520184241) do

  create_table "activities", force: true do |t|
    t.integer  "user_id"
    t.integer  "subject_id"
    t.string   "subject_type"
    t.string   "slug"
    t.integer  "page_id"
    t.string   "activity_str"
    t.integer  "real_subject_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["slug"], name: "index_activities_on_slug", using: :btree
  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "all_domains", force: true do |t|
    t.string   "domain"
    t.integer  "score",           default: 0,     null: false
    t.boolean  "video"
    t.string   "embed"
    t.integer  "width"
    t.integer  "height"
    t.boolean  "spam",            default: false
    t.boolean  "approved",        default: false
    t.string   "provider"
    t.string   "id_regex"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "blocks_count",    default: 0,     null: false
    t.integer  "spams_count",     default: 0,     null: false
    t.integer  "pages_count",     default: 0,     null: false
    t.integer  "noloading_count", default: 0,     null: false
    t.integer  "unlikes_count",   default: 0,     null: false
  end

  add_index "all_domains", ["domain"], name: "index_all_domains_on_domain", using: :btree

  create_table "channels", force: true do |t|
    t.string   "title"
    t.text     "about"
    t.integer  "user_id"
    t.string   "ctype"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pages_count",              default: 0, null: false
    t.integer  "followers_counter",        default: 0, null: false
    t.string   "cover_photo_file_name"
    t.string   "cover_photo_content_type"
    t.integer  "cover_photo_file_size"
    t.datetime "cover_photo_updated_at"
    t.string   "slug"
  end

  add_index "channels", ["slug"], name: "index_channels_on_slug", unique: true, using: :btree
  add_index "channels", ["user_id"], name: "index_channels_on_user_id", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "content"
    t.integer  "page_id"
    t.boolean  "spam",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["page_id"], name: "index_comments_on_page_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "follows", force: true do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "interest_suggestions", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interestings", force: true do |t|
    t.integer  "user_id"
    t.integer  "interest_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "interestings", ["user_id", "interest_id"], name: "index_interestings_on_user_id_and_interest_id", unique: true, using: :btree

  create_table "interests", force: true do |t|
    t.string   "name"
    t.boolean  "active",             default: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "pages_count",        default: 0,     null: false
    t.text     "description"
  end

  add_index "interests", ["name"], name: "index_interests_on_name", using: :btree
  add_index "interests", ["slug"], name: "index_interests_on_slug", unique: true, using: :btree

  create_table "likes", force: true do |t|
    t.integer  "user_id"
    t.integer  "page_id"
    t.boolean  "spam"
    t.boolean  "liked"
    t.integer  "interest_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "channel_id"
  end

  add_index "likes", ["page_id"], name: "index_likes_on_page_id", using: :btree
  add_index "likes", ["user_id", "interest_id"], name: "index_likes_on_user_id_and_interest_id", using: :btree
  add_index "likes", ["user_id", "page_id"], name: "index_likes_on_user_id_and_page_id", unique: true, using: :btree

  create_table "listings", force: true do |t|
    t.integer  "page_id"
    t.integer  "list_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "interest_id"
  end

  add_index "listings", ["list_id", "interest_id"], name: "index_listings_on_list_id_and_interest_id", using: :btree
  add_index "listings", ["page_id", "interest_id", "user_id"], name: "index_listings_on_page_id_and_interest_id_and_user_id", using: :btree
  add_index "listings", ["page_id", "list_id"], name: "index_listings_on_page_id_and_list_id", unique: true, using: :btree
  add_index "listings", ["user_id", "interest_id"], name: "index_listings_on_user_id_and_interest_id", using: :btree
  add_index "listings", ["user_id", "list_id"], name: "index_listings_on_user_id_and_list_id", using: :btree
  add_index "listings", ["user_id"], name: "index_listings_on_user_id", using: :btree

  create_table "lists", force: true do |t|
    t.string   "title"
    t.string   "about"
    t.integer  "user_id"
    t.boolean  "private",            default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pages_count",        default: 0,     null: false
    t.string   "cover_photo_url"
    t.integer  "followers_counter",  default: 0,     null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "slug"
    t.boolean  "main",               default: false
  end

  add_index "lists", ["slug"], name: "index_lists_on_slug", unique: true, using: :btree

  create_table "notifications", force: true do |t|
    t.integer  "resource_id"
    t.string   "resource_type"
    t.integer  "user_id"
    t.boolean  "closed",         default: false
    t.boolean  "viewed",         default: false
    t.integer  "actor_id"
    t.integer  "page_id"
    t.integer  "created_at_int"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "notifications", ["created_at_int"], name: "index_notifications_on_created_at_int", using: :btree
  add_index "notifications", ["user_id", "viewed"], name: "index_notifications_on_user_id_and_viewed", using: :btree

  create_table "page_views", force: true do |t|
    t.integer  "page_id"
    t.integer  "user_id"
    t.integer  "score"
    t.integer  "viewed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "page_views", ["user_id", "viewed_at"], name: "index_page_views_on_user_id_and_viewed_at", using: :btree

  create_table "pages", force: true do |t|
    t.integer  "user_id"
    t.string   "url"
    t.integer  "ptype"
    t.boolean  "published",                default: true
    t.boolean  "featured",                 default: false
    t.boolean  "spam",                     default: false
    t.string   "title"
    t.text     "about"
    t.string   "slug"
    t.string   "source_title"
    t.string   "source_description"
    t.integer  "interest_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "likes_count",              default: 0,     null: false
    t.integer  "unlikes_count",            default: 0,     null: false
    t.integer  "comments_count",           default: 0,     null: false
    t.integer  "shares_count",             default: 0,     null: false
    t.integer  "saves_count",              default: 0,     null: false
    t.integer  "intrest_id"
    t.string   "domain"
    t.integer  "channel_id"
    t.string   "cover_photo_file_name"
    t.string   "cover_photo_content_type"
    t.integer  "cover_photo_file_size"
    t.datetime "cover_photo_updated_at"
    t.integer  "score",                    default: 0,     null: false
    t.boolean  "domain_approved",          default: false
    t.text     "framecode"
    t.string   "embed_id"
    t.integer  "paged_at"
    t.integer  "all_domain_id"
    t.integer  "video_length"
    t.integer  "rand_order_no"
    t.text     "plain_about"
  end

  add_index "pages", ["channel_id"], name: "index_pages_on_channel_id", using: :btree
  add_index "pages", ["paged_at"], name: "index_pages_on_paged_at", using: :btree
  add_index "pages", ["plain_about"], name: "fulltext_pages_about", type: :fulltext
  add_index "pages", ["rand_order_no", "score", "paged_at", "id", "interest_id", "all_domain_id"], name: "rand_order_no_score_paged_at_id_interest_domain", using: :btree
  add_index "pages", ["slug"], name: "index_pages_on_slug", unique: true, using: :btree
  add_index "pages", ["title", "plain_about"], name: "fulltext_search_pages", type: :fulltext
  add_index "pages", ["title"], name: "fulltext_pages_title", type: :fulltext
  add_index "pages", ["user_id"], name: "index_pages_on_user_id", using: :btree

  create_table "photos", force: true do |t|
    t.integer  "page_id"
    t.string   "caption"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pattachment_file_name"
    t.string   "pattachment_content_type"
    t.integer  "pattachment_file_size"
    t.datetime "pattachment_updated_at"
  end

  add_index "photos", ["page_id"], name: "index_photos_on_page_id", using: :btree

  create_table "request_invites", force: true do |t|
    t.string   "name"
    t.text     "about"
    t.boolean  "author",         default: false
    t.text     "what_write"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "invite_sent"
    t.datetime "invite_sent_at"
    t.string   "email"
    t.boolean  "paid"
    t.string   "fb_link"
    t.string   "twitter_link"
  end

  create_table "shares", force: true do |t|
    t.integer  "user_id"
    t.string   "shared_via"
    t.text     "shared_emails"
    t.string   "share_entity"
    t.boolean  "shared"
    t.integer  "page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shares", ["page_id"], name: "index_shares_on_page_id", using: :btree
  add_index "shares", ["user_id"], name: "index_shares_on_user_id", using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree

  create_table "tags", force: true do |t|
    t.string "name"
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "username",               default: "",    null: false
    t.string   "slug"
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "channels_count",         default: 0,     null: false
    t.integer  "pages_count",            default: 0,     null: false
    t.integer  "comments_count",         default: 0,     null: false
    t.text     "blocked_pages"
    t.integer  "followers_counter",      default: 0,     null: false
    t.integer  "follows_counter",        default: 0,     null: false
    t.integer  "lists_count",            default: 0,     null: false
    t.integer  "likes_count",            default: 0,     null: false
    t.boolean  "male",                   default: true
    t.date     "birthday"
    t.boolean  "spam",                   default: false
    t.boolean  "author",                 default: false
    t.boolean  "admin",                  default: false
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
    t.boolean  "paid"
    t.datetime "last_paid_at"
    t.boolean  "invite_for_job",         default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

end
