class Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  acts_as_taggable
  has_many :listings
  has_many :lists, :through => :listings
  belongs_to :channel
  belongs_to :user
  has_many :comments
  belongs_to :interest
  has_many :likes
  after_validation :validate_reserved
  belongs_to :all_domain

  attr_reader :cover_photo_remote_url
  attr_accessor :do_not_update_score
  # attr_accessor :source_title
  # attr_accessor :source_description

  def self.random
    rand(18500)
  end

  after_create :record_activity
  accepts_nested_attributes_for :listings
  # validates :domain, :url, :title, :presence => true
  validates :interest_id, presence: true, numericality: { only_integer: true }
  validates :url, :format => URI::regexp(%w(http https)), :if => lambda { |p| p.url.present? }

  before_validation :add_domain_and_framecode
  before_create "self.paged_at=#{Time.now.to_i}"
  before_create "self.rand_order_no=#{Page.random}"

  counter_culture :user
  counter_culture :channel
  counter_culture :all_domain
  counter_culture :interest
  has_many :photos
  has_attached_file :cover_photo, :styles => { :thumb => "75x75#", 
    :medium => "440>x" }, :convert_options => {
    :medium => "-quality 75 -strip", :thumb => "-quality 75 -strip" }, :default_url =>  ":default_image_url"
  
   
  validates_attachment_content_type :cover_photo, :content_type => /\Aimage\/.*\Z/

  PTYPES = {
    "photo" => 3,
    "video" => 1,
    "story" => 2,
    "other" => 4
  }

  SCORES = {
    "like" => 1,
    "domain_approved" => 3,
    "list" => 2,
    "comment" => 2,
    "channel" => 20
  }


  def self.search(params={})
    page = params[:page] || 1
    per_page = 30
    offset = (page.to_i - 1) * per_page
    q = self.sanitize(params[:q])
    if params[:int].present?
      int_id = self.sanitize(params[:int]) 
    
      pages = find_by_sql("select *, MATCH (title) AGAINST (#{q}) AS rel1, MATCH (plain_about) AGAINST (#{q}) AS rel2 from pages where MATCH(title, plain_about) AGAINST (#{q}) and interest_id = #{int_id} ORDER BY (rel1*1.5)+(rel2) limit #{per_page} OFFSET #{offset}")
    else
      pages = find_by_sql("select *, MATCH (title) AGAINST (#{q}) AS rel1, MATCH (plain_about) AGAINST (#{q}) AS rel2 from pages where MATCH(title, plain_about) AGAINST (#{q}) ORDER BY (rel1*1.5)+(rel2) limit #{per_page} OFFSET #{offset}")
    end
    Page.eagerLoadFor(pages, :interest)
    count = find_by_sql("select COUNT(*) as ct from pages where MATCH(title, plain_about) AGAINST (#{q})").last.ct
    [pages, count]
  end

  def slug_path(params={})
    if(self.ptype == PTYPES["video"])
      URI.escape "/video/#{domain}/#{slug}"
    elsif (self.ptype == PTYPES["story"])
      URI.escape "/page/#{slug}"
    else
      URI.escape "/page/#{slug}"
    end
  end

  def User.update_photo_from_url
    Page.find_each do |p|
      unless p.cover_photo.present?
        begin
          p.cover_photo_remote_url = p.src_img_url
          p.save
        rescue Exception => e
          puts e.message
        end
      end
    end
  end

  def slug_url
     WEBSITE_NAME + slug_path
  end

  def cover_photo_remote_url=(url_value)
    self.cover_photo = URI.parse(url_value)
    @cover_photo_remote_url = url_value
  end

  def upadte_score(option)
    self.update_attribute("score", (self.score + SCORES[option.to_s])) unless self.do_not_update_score
  end

  def drop_score(option)
    self.update_attribute("score", (self.score - SCORES[option.to_s])) unless self.do_not_update_score
  end

  def title
    if self.ptype == PTYPES["video"]
      ptitle = self.read_attribute(:title)
      ptitle.present? ? ptitle : self.read_attribute(:source_title)
    else
      self.read_attribute(:title)
    end
  end

  def about(check_source =  true)
    if self.ptype == PTYPES["video"] && check_source
      pabout = self.read_attribute(:about)
      pabout.present? ? pabout : self.read_attribute(:source_description)
    else
      self.read_attribute(:about)
    end
  end

  def plain_about
    if self.ptype == PTYPES["video"]
      pabout = self.read_attribute(:plain_about)
      pabout.present? ? pabout : self.read_attribute(:source_description)
    else
      self.read_attribute(:plain_about)
    end
  end

protected
  def record_activity
    Activity.create_activity(self.user_id, self.channel_id, "channel", "channeled", self.id, self.id) if self.channel_id
  end

  def add_domain_and_framecode
    if self.url.present? && self.new_record?
      uri = PublicSuffix.parse(URI.parse(self.url).host)
      self.domain = uri.sld + "." + uri.tld
      dt = AllDomain.where(:domain => self.domain).first
      if dt && dt.spam
        self.errors.add(:url, "from #{domain} has been blacklisted. Please contact the administrator for more details.")
      end 
      if dt
        # TODO: Lets not add framecode for now, as we have to fetch the ALLDomin record anyway to make sure that we are filtering spam hosts
        self.generate_framecode(dt) if self.ptype == PTYPES["video"]
        self.domain_approved = true and self.score = score + SCORES["domain_approved"] if dt.approved
        self.all_domain_id = dt.id        
      elsif !dt
        self.all_domain = AllDomain.new(:domain => self.domain, :score => 0)
      end
    end
  end

  # def generate_framecode(all_domain)
  #   regexp = Regexp.new all_domain.id_regexp
  #   id = self.url.match(regexp)[0]
  #   all_domain.embed.gsub("substituteidhere", id)
  # end

  def generate_framecode(all_domain)
    res = RestClient.get self.url
    if res.code ==  200
      doc = Nokogiri::HTML.parse(res.body)
      if self.url.match(/you|vimeo/)
        doc.css("meta[property='og:image']")
        cover_img = doc.css("meta[property='og:image']").first.attr("content") if doc.css("meta[property='og:image']").first

        if self.url.match(/vimeo/)
          framec = doc.css("meta[itemprop='embedUrl']").first.attr("content") if doc.css("meta[itemprop='embedUrl']").first
        else
          framec = doc.css("link[itemprop='embedURL']").first.attr("href") if doc.css("link[itemprop='embedURL']").first
        end
        self.source_description = doc.css("meta[name='description']").first.attr("content") if doc.css("meta[name='description']").first.present?
        if framec.present?
          width = doc.css("meta[itemprop='width']").first.attr("content") if doc.css("meta[itemprop='width']").first
          height = doc.css("meta[itemprop='height']").first.attr("content") if doc.css("meta[itemprop='height']").first
          self.cover_photo_remote_url = cover_img if cover_img.present?
          self.framecode = "<iframe class='svideo' width='#{width || 640}' height='#{height || 480}' src='#{framec}' frameborder='0' allowfullscreen></iframe>"
        else
          self.errors.add(:base, "Oops! Video was not found.")
        end
      else
        self.errors.add(:base, "Only Vimeo and Youtube videos can be added. Please contact us to add video from any other source")
      end
    else
      self.errors.add(:base, "Could not access the video url! Please try again.")
    end
  end

  def validate_reserved
    slug
    rescue FriendlyId::BlankError
    rescue FriendlyId::ReservedError
    @errors[friendly_id_config.method] = "is reserved. Please choose something else"
    return false
  end
  
end
