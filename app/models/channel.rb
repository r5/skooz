class Channel < ActiveRecord::Base
  belongs_to :user
  acts_as_followable
  has_many :pages
  counter_culture :user

  extend FriendlyId
  friendly_id :title, use: :slugged


  validates :title, presence: true
  validates :about, presence: true

  has_attached_file :cover_photo, :styles => { :thumb => "75x75#", 
    :medium => "440>x" }, :convert_options => {
    :medium => "-quality 45 -strip", :thumb => "-quality 45 -strip", :original => "-quality 50 -strip" }, :default_url =>  ":default_image_url"
  
   
  validates_attachment_content_type :cover_photo, :content_type => /\Aimage\/.*\Z/
end
