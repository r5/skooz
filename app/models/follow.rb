class Follow < ActiveRecord::Base

  extend ActsAsFollower::FollowerLib
  extend ActsAsFollower::FollowScopes

  # NOTE: Follows belong to the "followable" interface, and also to followers
  belongs_to :followable, :polymorphic => true
  belongs_to :follower,   :polymorphic => true

  after_create :update_counters
  after_destroy :destroy_counters

  def block!
    self.update_attribute(:blocked, true)
  end

private

def update_counters
  follower = self.follower
  followable = self.followable
  follower.class.send("increment_counter", "follows_counter", follower.id) 
  followable.class.send("increment_counter", "followers_counter", followable.id)
end

def destroy_counters
  follower = self.follower
  followable = self.followable
  follower.class.send("decrement_counter", "follows_counter", follower.id) if follower.follows_counter > 0
  followable.class.send("decrement_counter", "followers_counter", followable.id) if followable.followers_counter > 0
end

end
