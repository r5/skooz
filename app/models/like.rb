class Like < ActiveRecord::Base
  belongs_to :user
  belongs_to :page
  counter_culture :page, :column_name => Proc.new { |page|
    if page.liked?
      "likes_count"
    elsif page.liked == false
      "unlikes_count"
    end
  }

  after_create :record_activity, :create_notification
  after_create :increase_score

  validate :liked, :user_id, :page_id, presence: true
  validates :user_id, uniqueness: {scope: :page_id}
  counter_culture :user, :column_name => "likes_count"
  
private
  def record_activity
    Activity.create_activity(self.user_id, self.user_id,"user",(self.liked ? "liked" : "unliked"), self.page_id, self.id)
  end

  def create_notification
    Notification.create_bulk_notification(self, "like")
  end

  def increase_score
    if self.liked
      self.page.upadte_score("like")
    else
      self.page.drop_score("like")
    end
  end
end
