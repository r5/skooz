class Listing < ActiveRecord::Base
  belongs_to :list
  belongs_to :user
  belongs_to :page
  

  validates :page_id, uniqueness: {scope: :list_id}
  after_create :record_activity, :create_notification, :add_photo_to_list
  after_create :increase_score
  counter_culture :list, :column_name => "pages_count"
  attr_accessor :do_not_update_page_score

private
  def record_activity
    Activity.create_activity(self.user_id, self.list_id,"list", "listed", self.page_id, self.id)
  end

  def create_notification
    Notification.create({:user_id => self.page.user_id, :resource_id => self.id, :resource_type => "listing", :page_id => self.page_id, :actor_id => self.user_id})
  end

  def increase_score
    self.page.upadte_score("list") unless self.do_not_update_page_score
  end

  def add_photo_to_list
    page = self.page
    list = self.list
    if page.cover_photo.present? && !list.photo.present?
      list.photo = page.cover_photo
      list.save
    end
  end
end
