class List < ActiveRecord::Base
  extend FriendlyId
  belongs_to :user
  acts_as_followable
  has_many :listings
  has_many :pages, :through => :listings
  friendly_id :title, use: :slugged
  counter_culture :user, :column_name => "lists_count"
  
  accepts_nested_attributes_for :listings

  validates :title, :presence => true

  has_attached_file :photo, :styles => { 
    :thumb => "100x100#" }, :convert_options => {
    :thumb => "-quality 75 -strip" }, :default_url => ":default_image_url"
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

end
