class RequestInvite < ActiveRecord::Base


  def invite(invite_for_job=false)
    rinvite = self
    ActiveRecord::Base.transaction do
      User.invite!(:email => self.email, :username => self.name , :author => self.author, :paid => self.paid, :invite_for_job => invite_for_job)
      rinvite.update_attributes(:invite_sent => true, :invite_sent_at => Time.now)
    end
  end
end
