class AllDomain < ActiveRecord::Base


  def remove_count(key)
    AllDomain.decrement_counter(key, self.id) if self.send(key) > 0
  end

  def add_count(key)
    AllDomain.increment_counter(key, self.id)
  end
end
