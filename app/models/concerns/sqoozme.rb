module Sqoozme
  extend ActiveSupport::Concern



  module ClassMethods
    def best_pages
      rand_no = Page.random
      score_now = [User::SQOOZING_SCORES[:top]].sample
      pages = Page.from("pages USE Index (rand_order_no_score_paged_at_id_interest_domain)").where("(rand_order_no > ? ) and score >= ? and paged_at > ?", rand_no, score_now, 4.months.ago.to_i).limit(6)
    end
    def sqoozitnow(user, params={})
      rand_no = Page.random
      score_now = [User::SQOOZING_SCORES[:top] ].sample
      main_interest_id = params["int"].present? ? params["int"].to_i : nil
      if user
        start = 10.days.ago.to_i
        endt =Time.now.to_i
        page_views = user.page_views.where(:viewed_at => start..endt)
        last_day = []#page_views.collect{|p| p if p.viewed_at > start and p.viewed_at < endt}.compact
        blocked_pages = user.blocked_pages
         blocked_page_ids = []
        blocked_page_ids = blocked_pages.keys.map{|k| blocked_pages[k] unless k == "domain_ids"}.flatten
        blocked_domains = []
        blocked_domains = blocked_pages["domain_ids"].compact if !blocked_pages["domain_ids"].nil?
        blocked_domains = (blocked_domains + AllDomain.where(:spam => true).map(&:id)).uniq
        blocked_page_ids = (page_views.map(&:page_id) + blocked_page_ids).compact
        # Domain which are spamed should not be shown in thi
        blocked_page_ids = blocked_page_ids.uniq
        rand_no = Page.random

        if last_day.empty?
          if(main_interest_id || [true,false].sample)
            # True false from this condition decides whether to show a pupular page or a less popular page
            interest_ids = main_interest_id ? [main_interest_id] : user.interest_ids
            if blocked_domains.present?
              page = Page.from("pages USE Index (rand_order_no_score_paged_at_id_interest_domain)").where("(rand_order_no < ? ) and score >= ? and paged_at > ?", rand_no, score_now, 4.months.ago.to_i).where.not(:id =>  blocked_page_ids).where(:interest_id => interest_ids).where.not(:all_domain_id => blocked_domains).order(" rand_order_no desc").first
            else
              page = Page.from("pages USE Index (rand_order_no_score_paged_at_id_interest_domain)").where("(rand_order_no < ? ) and score >= ? and paged_at > ?", rand_no, score_now, 4.months.ago.to_i).where.not(:id =>  blocked_page_ids).where(:interest_id => interest_ids).order(" rand_order_no desc").first
            end
          else
            
            if blocked_domains.present?
              page = Page.from("pages USE Index (rand_order_no_score_paged_at_id_interest_domain)").where("(rand_order_no < ? ) and score >= ? and paged_at > ?", rand_no, score_now, 4.months.ago.to_i).where.not(:id =>  blocked_page_ids).where.not(:all_domain_id => blocked_domains).order(" rand_order_no desc").first
            else
              page = Page.from("pages USE Index (rand_order_no_score_paged_at_id_interest_domain)").where("(rand_order_no < ? ) and score >= ? and paged_at > ?", rand_no, score_now, 4.months.ago.to_i).where.not(:id =>  blocked_page_ids).order(" rand_order_no desc").first
            end
          end
        else
          # Dont know why I wrote that last day thingie, but lets think abt it later. I can't racall right now
        end
       
      else
        page = Page.from("pages USE Index (rand_order_no_score_paged_at_id_interest_domain)").where("(rand_order_no < ?) and score >= ? and paged_at > ?", rand_no, score_now, 4.months.ago.to_i).where(main_interest_id ? {:interest_id => [main_interest_id]} : "").order(" rand_order_no desc").first
      end
      unless page
        page = Page.from("pages USE Index (rand_order_no_score_paged_at_id_interest_domain)").where("(rand_order_no > ? ) and score >= ? and paged_at > ?", rand_no, score_now, 4.months.ago.to_i).where(main_interest_id ? {:interest_id => [main_interest_id]} : "").order(" rand_order_no desc").first
      end
      page
    end 
  end
  



end