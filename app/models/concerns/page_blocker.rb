module PageBlocker
  extend ActiveSupport::Concern

  included do

  end 
  

  def not_for_me(page)
    bp = self.blocked_pages
    if !bp["not_for_me"].present? || !bp["not_for_me"].include?(page.id)
      !bp["not_for_me"].nil? ? bp["not_for_me"] << page.id : bp["not_for_me"] = [page.id]
      self.update_attribute("blocked_pages", bp)
    end
  end

  def yes_for_me(page)
    bp = self.blocked_pages
    if bp["not_for_me"].present? && bp["not_for_me"].include?(page.id)
      bp["not_for_me"].delete(page.id)
      self.update_attribute("blocked_pages", bp)
    end
  end

  def seen_it_already(page)
    bp = self.blocked_pages
    if !bp["seent_it"].present? || !bp["seent_it"].include?(page.id)
      !bp["seent_it"].nil? ? bp["seent_it"] << page.id : bp["seent_it"] = [page.id]
      self.update_attribute("blocked_pages", bp)
    end
  end

  def unseen_it_already(page)
    bp = self.blocked_pages
    if bp["seent_it"].present? && bp["seent_it"].include?(page.id)
      bp["seent_it"].delete(page.id)
      self.update_attribute("blocked_pages", bp)
    end
  end

  def report_page_no_load(page)
    bp = self.blocked_pages
    if !bp["noloading"].present? || !bp["noloading"].include?(page.id)
      !bp["noloading"].nil? ? bp["noloading"] << page.id : bp["noloading"] = [page.id]
      self.update_attribute("blocked_pages", bp)
      page.all_domain.add_count("noloading_count")
    end
  end

  def page_does_load(page)
    bp = self.blocked_pages
    if bp["noloading"].present? && bp["noloading"].include?(page.id)
      bp["noloading"].delete(page.id)
      self.update_attribute("blocked_pages", bp)
      page.all_domain.remove_count("noloading_count")
    end
  end

  def report_spam(page)
    bp = self.blocked_pages
    if !bp["spam_ids"].present? || !bp["spam_ids"].include?(page.id)
      !bp["spam_ids"].nil? ? bp["spam_ids"] << page.id : bp["spam_ids"] = [page.id]
      self.update_attribute("blocked_pages", bp)
      page.all_domain.add_count("spams_count")
    end
  end

  def report_unspam(page)
    bp = self.blocked_pages
    if bp["spam_ids"].present? && bp["spam_ids"].include?(page.id)
      bp["spam_ids"].delete(page.id)
      self.update_attribute("blocked_pages", bp)
      page.all_domain.remove_count("spams_count")
    end
  end

  def block_this_page(page)
    bp = self.blocked_pages
    if !bp["domain_ids"].present? || !bp["domain_ids"].include?(page.all_domain_id)
      !bp["domain_ids"].nil? ? bp["domain_ids"] << page.all_domain_id : bp["domain_ids"] = [page.all_domain_id]
      self.update_attribute("blocked_pages", bp)
      page.all_domain.add_count("blocks_count")
    end
  end

  def unblock_this_page(page)
    bp = self.blocked_pages
    if bp["domain_ids"].present? && bp["domain_ids"].include?(page.all_domain_id)
      bp["domain_ids"].delete(page.all_domain_id)
      self.update_attribute("blocked_pages", bp)
      page.all_domain.remove_count("blocks_count")
    end
  end

end