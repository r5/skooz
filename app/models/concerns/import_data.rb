module ImportData
  extend ActiveSupport::Concern

 

  module ClassMethods
    def import_from_file(filename=nil)
      f = File.open("/home/vipin/sqooz_dumps/pushes/videos_on_18_apr.txt")
      b = f.readlines.map{|n| JSON.parse(n.chomp)}
      b.in_groups_of(1000).each do |gruop|
        pages = gruop.map do |g|
          Page.new(g)
        end
        import(pages)
      end
    end

    def remove_duplicate_title_pages
      tits = Page.select("*, Count(*) as c").group("title").having("c > 1").map(&:title).uniq
      deleted = [14214]
      c = 0
      tits.each do |tit|
        pages = Page.where(:title => tit)
        pages.each_with_index do |p, i|
          deleted << (p.destroy).id if i > 0
          # c = c + 1 if i > 0
        end
      end
      puts c
      puts "====================================="
      p deleted
    end

    def add_imported_pages_to_the_lists
      User.select("distinct users.*").joins(:lists).where("users.id not in (?)", [3345, 3030, 3559]).each do |u|
        pages = Page.where("paged_at > ?", 4.months.ago.to_i).where("interest_id != 307").where(:user_id => nil).limit(60).order("rand()")
        pages.each do |p|
          u.listings.create(:list_id => u.lists.first.id, :page_id => p.id, :interest_id => p.interest_id)
          p.save # To generate the slug
        end
        pages.update_all(:user_id => u.id)
      end
    end

    def add_interests_to_users
      User.all.each do |u|
        ints = u.interestings.map(&:interest_id)
        if ints.present?
          ints = []#Interest.where("id not in (?)", ints).where("pages_count >= 20").order("rand()").limit(10)
        else
          ints = Interest.where("pages_count >= 20").order("rand()").limit(10)
        end
        ints.each do |int|
          Interesting.create(:user_id => u.id, :interest_id => int.id)
        end
      end
    end

    def follow_some_users
      User.all.each do |u|
        following_ids = Follow.select("followable_id").where(:follower_id => u.id, :follower_type => "User", :blocked => false , :followable_type => "User").map(&:followable_id)
        following_ids << u.id
        users = User.select("distinct users.*").joins(:pages).where("users.id not in (?)", following_ids ).order("rand()").limit(10)
        
        users.each do |fu|
          u.follow(fu)
        end
      end
    end

    def follow_some_lists
      User.all.each do |u|
        following_ids = Follow.select("followable_id").where(:follower_id => u.id, :follower_type => "User", :blocked => false , :followable_type => "List").map(&:followable_id)
        if following_ids.present?
          lists = List.where("id not in (?)", following_ids).where("pages_count >= 20").order("rand()").limit(10)
        else
          lists = List.where("pages_count >= 20").order("rand()").limit(10)
        end
        lists.each do |l|
          u.follow(l)
        end
      end
    end
    def add_random_number
      c = Page.count
      Page.find_each do |p|
        p.update_attribute("rand_order_no", rand(c))
      end
    end

    def add_images
      User.find_each do |u|
        unless u.avatar.present?
          p = Page.order("rand()").first
          u.update_attributes(:avatar => File.open(File.expand_path(File.join(File.dirname(__FILE__), '..', '..', "shared/", p.photo(:original).gsub(/\?.+/, "") )), 'rb'))
        end
      end

    end

    def like_some_videos
      User.all.each do |u|
        likes = u.likes.map(&:page_id)
        if likes.present?
          pages = Page.where("id not in (?)", likes).order("rand()").limit((150..190).to_a.sample)
        else
          pages = Page.order("rand()").limit((150..190).to_a.sample)
        end
        pages.each do |p|

          u.like(p)
        end
      end
    end
  end


  



end

