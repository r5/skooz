module Feed
  extend ActiveSupport::Concern

  included do

  end

  module ClassMethods
    def feed_trending(params)
      # TODO: Add index to likes count
      @pages = Page.page(params[:page] || 1).order("likes_count desc").includes(:interest)
    end

    def feed_trending_lists(params)
      # TODO: Add index here && lists should not be followed by user already
      @lists = List.page(params[:page] || 1).order("followers_counter desc, pages_count desc").includes(:user)
    end

    def feed_trending_channels(params)
      # TODO: Add index here && lists should not be followed by user already
      @channels = Channel.page(params[:page] || 1).order("followers_counter desc, pages_count desc").includes(:user)
    end
  end
  
  def recommended_pages(params)
    # Add index here
    interest_ids = self.interestings.map(&:interest_id)
    Page.where(:interest_id => interest_ids).page(params[:page] || 1).order("pages.likes_count").includes(:interest)
    # listings = Listing.select("distinct page_id").from("listings use index (index_listings_on_page_id_and_interest_id_and_user_id)").joins(:page).where("listings.interest_id in (?) and listings.user_id != ?", interest_ids, self.id).order("pages.likes_count").page(params[:page] || 1).includes({:page => :interest})
  end

  def feed_activity(params)
    generate_index = generate_index_arr
    Activity.where(:slug => generate_index).includes([:user,{:page => :interest}]).page(params[:page] || 1).order("id desc")
  end



  def generate_index_arr
    Follow.where(:follower_id => self.id).map{|f| f.followable_id.to_s + f.followable_type[0]}
  end

 
end