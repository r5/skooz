class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: :slugged
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar, :styles => {:thumb => "100x100#"}, :convert_options => {
    :thumb => "-quality 75 -strip" }, :default_url =>  ":default_image_url"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  PTYPES = {
    "photo" => 1,
    "video" => 2,
    "story" => 3,
    "other" => 4
  }
  if Rails.env == "development"
    ActiveRecord::Base.logger = Logger.new(STDOUT)
  end

  validates :username, :presence => true


  after_validation :validate_reserved
  acts_as_followable
  acts_as_follower
  acts_as_mappable
  acts_as_tagger

  has_many :pages
  has_many :lists
  has_many :channels
  has_many :likes
  has_many :shares
  has_many :listings
  has_many :comments
  has_many :activities
  has_many :interestings
  has_many :interests, :through => :interestings
  has_many :notifications
  has_many :page_views
  serialize :blocked_pages, Hash

  before_save :delete_latlng

  include Sqoozme
  include PageBlocker
  include Feed
  include ImportData

  SQOOZING_SCORES = {
    :exceptional => 6000,
    :top => 2000,
    :best => 1000,
    :better => 500,
    :good => 250,
    :start => 100,

  }

  SQOOZING_CYCLE = [["top", ""]]

  def validate_reserved
    slug
    rescue FriendlyId::BlankError
    rescue FriendlyId::ReservedError
    @errors[friendly_id_config.method] = "is reserved. Please choose something else"
    return false
  end

  def like(page)
    like = Like.find_or_initialize_by(:user_id => self.id, :page_id => page.id)
    like.interest_id = page.interest_id
    like.liked =  true
    like.save
  end

  def unlike(page)
    like = Like.find_or_initialize_by( :user_id => self.id, :page_id => page.id)
    like.interest_id = page.interest_id
    like.liked =  false
    like.save
  end

  def js_page_data(page)
    ulists = self.lists.select("id, title")
    {:lists => ulists}
  end

  def viewed_page(page)
    PageView.create(:page_id => page.id, :user_id => self.id, :score => page.score)
  end

  def all_likes_interests
    Interest.where(:id => self.likes.where(:liked => true).select("distinct interest_id").map(&:interest_id))
  end

  def all_likes_channels
    Channel.where(:id => self.likes.select("distinct channel_id").map(&:channel_id))
  end

  def all_listing_interests
    Interest.where(:id => self.listings.select("distinct interest_id").map(&:interest_id))
  end

  def filtered_likes(params)
    if(params[:filter])
      # TODO: remove index. This is no longer required as there are just videos
      # Likes.where("liked" => true).where("user_id = ? and pages.ptype = ?", self.id, Page.PTYPES[params[:filter]]).order("updated_at desc").page(params[:page] || 1).includes({:page => "interest"})
    elsif params[:iname]
      interest_id = Interest.friendly.find(params[:iname]).try(:id)
      self.likes.select("id, page_id").from("likes use index (index_likes_on_user_id_and_interest_id)").where(:interest_id => interest_id, :liked => true).order("updated_at desc").includes({:page => "interest"}).page params[:page]
    elsif params[:cname]
      # Add index here
      channel_id = Channel.friendly.find(params[:cname]).try(:id)
      self.likes.select("id, page_id").from("likes").where(:channel_id => channel_id, :liked => true).order("updated_at desc").includes({:page => "interest"}).page params[:page]
    elsif params[:ptype]
      ptype = Page::PTYPES[params[:ptype]]
      self.likes.select("id, page_id").from("likes").where(:ptype => ptype, :liked => true).order("updated_at desc").includes({:page => "interest"}).page params[:page]
    else
      self.likes.select("id, page_id").from("likes").order("updated_at desc").where(:liked => true).includes({:page => "interest"}).page params[:page]
    end
  end

  def filtered_pages(params)
    if(params[:filter]) # Filter should always by indicated as list
      # No Ptype for now
      list_id = List.friendly.find(params[:filter]).try(:id)
      self.listings.from("listings use INDEX (index_listings_on_user_id_and_list_id)").where(:list_id => list_id).order("updated_at desc").includes(["list",{"page" => "interest"}]).page(params[:page] || 1)
    elsif params[:iname]
      interest_id = Interest.friendly.find(params[:iname]).try(:id)
      self.listings.from("listings USE INDEX (index_listings_on_user_id_and_interest_id)").where(:interest_id => interest_id).group("page_id").order("updated_at desc").includes(["list",{"page" => "interest"}]).page(params[:page] || 1)
    else
      # TODO: Make sure group_by performs well with lot of data
      self.listings.group("page_id").order("updated_at desc").includes(["list",{"page" => "interest"}]).page(params[:page] || 1)
    end
  end
  
  def interests_to_follow
    allready_added_ids = Interesting.from("interestings").where(:user_id => self.id).map(&:interest_id)
    all_interests = Interest.active.where("pages_count > 0").order("name")
    [allready_added_ids, all_interests]
  end

  def channels_to_follow(params)
    allready_added_ids = Follow.where(:follower_id => self.id, :follower_type => "User", :blocked => false , :followable_type => "channel").map(&:followable_id) + self.channels.map(&:id)
    # Update this after we have good number of channels
    Channel.where("channels.id not in (?)", allready_added_ids ).order(["followers_counter", "pages_count", "user_id", "id", "updated_at"].sample).includes(:user).page(params[:page] || 1).includes(:user)
  end

  def lists_to_follow(params) 
    # TODO make it geographically sorted as per its user when we have some data
    # List.select("distinct lists.*").where(:id => Listing.select("distinct listings.list_id").from("listings USE INDEX (index_listings_on_list_id_and_interest_id)").where("listings.list_id not in (?)", Follow.select("followable_id").where(:follower_id => self.id, :follower_type => "User", :blocked => false , :followable_type => "List")).where(:interest_id => Interesting.select("id").from("interestings USE INDEX (index_interestings_on_user_id)").where(:user_id => self.id))).order([true, true, false].sample ? "lists.pages_count" : nil)

    # Lets just show all the lists as initially we will not have lot of lists to show
    allready_added_ids = Follow.where(:follower_id => self.id, :follower_type => "User", :blocked => false , :followable_type => "list").map(&:followable_id) + self.lists.map(&:id)
    List.where("lists.id not in (?)", allready_added_ids ).order(["followers_counter", "pages_count", "user_id", "id", "updated_at"].sample).includes(:user).page(params[:page] || 1)
  end

  def users_to_follow(params)
    # TODO make it geographically sorted as per its user when we have some data
    # if [false, true, true].sample
    #     User.select("distinct users.*").joins(:interestings).order("pages_count").page(params[:page] || 1)
    # else
    User.select("distinct users.*").joins(:pages).where("users.id not in (?)", Follow.select("followable_id").where(:follower_id => self.id, :follower_type => "User", :blocked => false , :followable_type => "User")).order("pages_count").page(params[:page] || 1)
    # end
  end

  def self.top_users(params)
    # TODO make it geographically sorted as per its user when we have some data
    # if [false, true, true].sample
    #     User.select("distinct users.*").joins(:interestings).order("pages_count").page(params[:page] || 1)
    # else
    User.order("pages_count").page(params[:page] || 1)
    # end
  end
# NOTE: I have deleted the Atrributes for locationg from users, add them back before using it
  def near(options={})
    mappable =  self
    per_page = 20
    page = options[:page] || 1
    bound = Geokit::Bounds.from_point_and_radius(mappable, 5000)
unless options[:interest_ids]
sql = <<SQL
select *, GLength(LineStringFromWKB(LineString((latlng), ((GeomFromText('Point(#{mappable.lat} #{mappable.lng})')))))) * 69 as distance from users where MBRWithin(latlng, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))')) order by distance asc limit #{per_page} offset #{(page.to_i - 1) * per_page};
SQL
else
order_by = [true, false, true].sample ?  "order by pages_count" : ''
sql = <<SQL
select * from users INNER JOIN interestings on users.id = interestings.user_id where interestings.interest_id in (#{options[:interest_ids].join(',')}) and users.id not in (select follows.followable_id from follows where follows.followable_type = 'user' and follower_id = #{mappable.id}) and users.id in (select id from users USE INDEX (users_spatial_idx) where MBRWithin(latlng, GeomFromText('Polygon((#{bound.ne.lat} #{bound.ne.lng}, #{bound.ne.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.sw.lng},#{bound.sw.lat} #{bound.ne.lng},#{bound.ne.lat} #{bound.ne.lng}))'))) #{order_by} limit #{per_page} offset #{(page.to_i - 1) * per_page};
SQL
end
# interestings.interest_id as interest_id 
# and interestings.interest_id in (#{options[:interest_ids].join(',')})
    User.find_by_sql(sql)
  end

private
  
  def delete_latlng
    @attributes.delete("latlng")
  end

end
