class PageView < ActiveRecord::Base
  before_create "self.viewed_at = #{Time.now.to_i}"
  belongs_to :user
  belongs_to :page

end
