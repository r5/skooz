class Activity < ActiveRecord::Base
  belongs_to :user
  belongs_to :page
  belongs_to :subject, polymorphic: true

  def self.create_activity(user_id, subject_id, subject_type, message, page_id, rsid)
    Activity.create({:user_id => user_id, :subject_id => subject_id, :subject_type => subject_type, :activity_str => message, :page_id => page_id, :real_subject_id => rsid, :slug => (subject_id.to_s) + subject_type[0]})
  end
end
