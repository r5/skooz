class Interest < ActiveRecord::Base

  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :pages
  has_many :interestings
  has_many :users, :through => :interestings

  scope :active, -> { where(active: true) }
end
