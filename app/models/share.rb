class Share < ActiveRecord::Base
  serialize :shared_emails, Array
  belongs_to :user
  belongs_to :page
  counter_culture :page
end
