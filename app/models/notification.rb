class Notification < ActiveRecord::Base
  before_save "self.created_at_int = #{Time.now.to_i}"
  scope :pending, ->  {where(:viewed => false)}

  scope :latest, -> {order("created_at_int desc")}

  belongs_to :page
  belongs_to :actor, :foreign_key => "actor_id", :class_name => "User"
  
  def self.create_bulk_notification(resource, resource_type)
    begin
    user_ids = resource.page.lists.map(&:user_id).uniq
    if user_ids.length > 1
      notifications = user_ids.map do |uid|
        Notification.new({:user_id => uid, :resource_id => resource.id, :resource_type => resource_type, :page_id => resource.page_id, :actor_id => resource.user_id})
      end
      Notification.import(notifications)
    else
       Notification.create({:user_id => resource.page.user_id, :resource_id => resource.id, :resource_type => resource_type, :page_id => resource.page_id, :actor_id => resource.user_id})
    end
    rescue Exception => e
      Rails.logger.error e.message
    end
  end
end
