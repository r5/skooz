class Photo < ActiveRecord::Base
  has_attached_file :pattachment,  :convert_options =>{ :original => "-quality 50 -strip" }, :default_url =>  ":default_image_url"
  validates_attachment_content_type :pattachment, :content_type => /\Aimage\/.*\Z/
end
