class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :page
  counter_culture :page
  counter_culture :user

  scope :active, -> { where(spam: false) }

  after_create :record_activity, :create_notification
  after_create :increase_score

  validates :content, :presence => true

private
  def record_activity
    Activity.create_activity(self.user_id, self.user_id,"user", "commented", self.page_id, self.id)
  end

  def create_notification
    Notification.create_bulk_notification(self, "comment")
  end

  def increase_score
    self.page.upadte_score("comment")
  end
end
