module UsersHelper
  include ActsAsTaggableOn::TagsHelper



  def follow_unfollow_link(resource_type, resource_id, follow=true, cls=nil)
    if current_user
      if(follow)
        ("<div class='btn-group-vertical #{cls}'>" +  link_to("Follow", "javascript:void(0)" , :onclick => "follow('#{resource_type}', #{resource_id}, $(this))", :class => " follow btn  btn-primary ") +     link_to("Unfollow", "javascript:void(0)", :disabled => true, :onclick => "unfollow('#{resource_type}', #{resource_id}, $(this))", :class => "btn  btn-default unfollow ") +    '</div>').html_safe
      else
        ("<div class='btn-group-vertical #{cls}'>" +  link_to("Follow", "javascript:void(0)" , :onclick => "follow('#{resource_type}', #{resource_id}, $(this))", :disabled => true, :class => "follow btn btn-primary ") +     link_to("Unfollow", "javascript:void(0)", :onclick => "unfollow('#{resource_type}', #{resource_id}, $(this))", :class => "btn  btn-default unfollow ") +    '</div>').html_safe
      end
    else
      # ("<div class='btn-group-vertical #{cls}'>" +  link_to("Follow", "javascript:void(0)" , :onclick => "follow('#{resource_type}', #{resource_id}, $(this))", :class => "login btn  btn-primary ") +     link_to("Unfollow", "javascript:void(0)", :onclick => "unfollow('#{resource_type}', #{resource_id}, $(this))", :class => " login btn  btn-default unfollow ") +    '</div>').html_safe
      ("<div class='btn-group-vertical #{cls}'>" +  link_to("Follow", "javascript:void(0)" , :class => "login btn  btn-primary ") +     link_to("Unfollow", "javascript:void(0)", :class => " login btn  btn-default unfollow ") +    '</div>').html_safe
    end
  end

  def follow_unfollow_small_link(resource_type, resource_id, follow=true, cls=nil)
    if current_user
      if(follow)
        ( link_to("Follow", "javascript:void(0)" , :onclick => "follow('#{resource_type}', #{resource_id}, $(this))", :class => "follow jlink")).html_safe
      else
        (   link_to("Unfollow", "javascript:void(0)", :onclick => "unfollow('#{resource_type}', #{resource_id}, $(this))", :class => " unfollow jlink")).html_safe
      end
    else
      # ("<div class='btn-group-vertical #{cls}'>" +  link_to("Follow", "javascript:void(0)" , :onclick => "follow('#{resource_type}', #{resource_id}, $(this))", :class => "login btn  btn-primary ") +     link_to("Unfollow", "javascript:void(0)", :onclick => "unfollow('#{resource_type}', #{resource_id}, $(this))", :class => " login btn  btn-default unfollow ") +    '</div>').html_safe
      (link_to("Follow", "javascript:void(0)" , :class => "login") ).html_safe
    end
  end
end
