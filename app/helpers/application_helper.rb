module ApplicationHelper

  def flash_class(level)
    case level
    when :notice then "alert-info"
    when :error then "alert-danger"
    when :alert then "alert-warning"
    end
  end

  def link_to_iframe(text, path, params={})
    link_to(text, myframe_path(:to => URI.escape(path)), :class => params[:class])
  end

  def sample_sharing_quote
    quote = [{:text => "Friendship marks a life even more deeply than love. Love risks degenerating into obsession, friendship is never anything but sharing.", :author => "Elie Wiesel"},{:text => "Happiness quite unshared can scarcely be called happiness; it has no taste.", :author => "Charlotte Brontë"}, {:text => "It ain't no fun if the homies can't have none.", :author => "Snoop Dogg"}, {:text => "Love only grows by sharing. You can only have more for yourself by giving it away to others.", :author => "Brian Tracy"}, {:text => "You are forgiven for your happiness and your successes only if you generously consent to share them.", :author => "Albert Camus"}].sample
    "<blockquote><p>#{quote[:text]}</p><footer><cite title='Source Title'>#{quote[:author]}</cite></footer></blockquote>".html_safe
  end

  def link_to_active(name, path, params={})
    params[:class] = params[:class].to_s + " active" if request.fullpath ==  path
    link_to(name, path, params).html_safe
  end

  def can?(action, object)
    if current_user
      object.user_id == current_user.id
    else
      return false
    end
  end
end
