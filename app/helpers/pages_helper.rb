module PagesHelper
   include ActsAsTaggableOn::TagsHelper

  def get_framecode(page, source=nil)
    if page.embed_id
      source = AllDomain.where(:domain => page.domain).first
      if source && source.embed.present? and page.embed_id.present?
        return source.embed.gsub("substituteidhere", page.embed_id)
      end
    end
    return nil
  end

  def get_color(index)
    ["turquoise", "emerland", "peter-river", "amethyst", "wet-asphalt", "green-sea", "nephritis", "belize-hole", "wisteria", "midnight-blue", "sun-flower", "carrot", "alizarin", "concrete", "orange", "pumpkin", "pomegranate", "silver", "asbestos"][index]
  end

  def get_navbar_sprinkle
    ("<ul class='navbar-sprinkle list-unstyled'>" +["turquoise","peter-river", "amethyst","green-sea", "nephritis", "belize-hole", "midnight-blue", "sun-flower", "carrot", "alizarin", "clouds", "concrete", "orange", "pomegranate"].shuffle.map{|c| "<li class='#{c}'></li>"}.join("")  +"</ul>").html_safe
  end

  def link_to_blockers(name, path, key, page)
    ids = current_user.blocked_pages[key]
    pid = (key == "domain_ids" ? page.all_domain_id : page.id)
    if ids.present? && ids.include?(pid)
      link_to(name, path+"&reverse=true", :remote => true, :method => "post", :class => "performed",data: { :disable_with => name, :type => "json"})
    else
      link_to(name, path, :remote => true, :method => "post", data: { :disable_with => name , :type => "json"})
    end
  end

  def about_page(page)
    page.about.gsub('script', '').html_safe if page.about
    # sanitize(page.about)
    # Sanitize.clean((page.about) || "", Sanitize::Config::RELAXED).html_safe
  end

  def about_page_trunc(page)
    truncate(page.plain_about, :length => 300)
  end

  def notification_text(notification)
    user = notification.actor
    page =  notification.page
    if notification.resource_type == "like"
      str =  "liked your page #{link_to page.title, page_path(page), :class => "special-hover-link"}"
    elsif notification.resource_type == "comment"
     str =  ( link_to( "commented", page.slug_path + "?comment=#{notification.resource_id}" , :class => "special-hover-link") + " on " + link_to(page.title, page.slug_path, :class => "special-hover-link") + ".")
    elsif notification.resource_type == "listing"
     str = "added #{link_to page.title, page_path(page), :class => "special-hover-link"} to a list."
    end  
    str.html_safe 
  end

  def activity_text(activity)
    user = activity.user
    page =  activity.page
    (link_to(user.username, user_path(user), :class => "special-hover-link") + " " + activity_token(activity)).html_safe
  end

  def activity_token(activity)
    activity_str = activity.activity_str
    if activity_str == "liked"
      return "liked this."
    elsif activity_str == "commented"
     return ( link_to( "commented", "javascript:void(0)", :class => "comment-on-page special-hover-link") + " on this.")
    elsif activity_str == "listed"
     return "added this to list."
    end        
  end
end
