class Notifier < ActionMailer::Base
  default from: "support@sqooz.it"

  def invite_request(invite)
    @invite = invite
    mail(to: "vipin.itm@gmail.com", subject: 'New Invitation Requested')
  end


  def notify_support(sub, body)
    @body = body
    mail(to: "vipin.itm@gmail.com", subject: sub)
  end
end
