// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function(){

  // // TODO: make validation work on edit page
  if(!gon.user){
    window.validator = new FormValidator($(".new_user").attr("id"), [{
      name: 'user[email]',
      rules: 'required|valid_email'
    },
    {
      name: 'user[username]',
      rules: 'required'
    },
    {
        name: 'user[password]',
        rules: 'required'
    },
    {
        name: 'user[name]',
        rules: 'required'
    },
    {
        name: 'user[password_confirmation]',
        rules: 'required|matches[user_password]'
    }], function(errors, event) {
      $("#new_user:visible .has-error").removeClass("has-error");
      if (errors.length > 0) {

        $.each(errors, function(index, error){
          $('[name="'+ error.name +'"]').parents(".form-group").addClass("has-error");
        });
      }
      else{
       
      }
        
    });

    window.validator = new FormValidator($(".user_signin").attr("id"), [{
      name: 'user[email]',
      rules: 'required|valid_email'
    }, {
        name: 'user[password]',
        rules: 'required'
    }], function(errors, event) {
      $("#new_user:visible .has-error").removeClass("has-error");
      if (errors.length > 0) {
        $.each(errors, function(index, error){
          $('[name="'+ error.name +'"]').parents(".form-group").addClass("has-error");
        });
      }
      else{
       
      }
        
    });

    window.validator = new FormValidator($(".forgot_password").attr("id"), [{
      name: 'user[email]',
      rules: 'required|valid_email'
    }], function(errors, event) {
      $("#new_user:visible .has-error").removeClass("has-error");
      if (errors.length > 0) {
        $.each(errors, function(index, error){
          $('[name="'+ error.name +'"]').parents(".form-group").addClass("has-error");
        });
      }
      else{
       
      }
        
    });

    window.validator = new FormValidator($(".edit_password").attr("id"), [
      {
        name: 'user[password_confirmation]',
        rules: 'required|matches[password]'
      },
      {
        name: 'user[password]',
        rules: 'required'
      }
    ], function(errors, event) {
      $("#new_user:visible .has-error").removeClass("has-error");
      if (errors.length > 0) {
        $.each(errors, function(index, error){
          $('[name="'+ error.name +'"]').parents(".form-group").addClass("has-error");
        });
      }
      else{
       
      }
        
    });
  }
  // // Form validation ends here
});
