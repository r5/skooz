 // Comment modal
function likePage(like){
  $.ajax({
    url: "/likes",
    type: "post",
    data: {page: gon.page.slug, like: like},
    success: function(data){
      if(like){
        $("a#unlike-page, li#unlike-page a").removeClass("yellow");
        $("a#like-page, li#like-page a").addClass("yellow");
      }else{
        $("a#like-page, li#like-page a").removeClass("yellow");
        $("a#unlike-page, li#unlike-page a").addClass("yellow"); 
      }
    },
    error: function(){
      showFlashMessge("OOps! There was an error. Please try again later.")
    }
  });
}

function showSqoozPrompt(msg){
  window.sqoozPromtShown = true
  if(!detectmob()){
    $("#sqooz-prompt").html("<p>" + msg + "</p>").fadeIn();
    setTimeout(function(){
      $("#sqooz-prompt").fadeOut()
    }, 5000)
  }
}

function onCommentSubmitEvent(){
  $('form.new_comment').on('ajax:success', function() {
    $('form.new_comment .text-danger,form.new_comment .text-success').remove();
    if(!$("#page_comments .modal-header #table-scroll")[0]){
      $("#page_comments .modal-header").append('<div id="table-scroll"><ul class="media-list"></ul></div>')
    }
    $('form.new_comment').prepend('<p class="bg-success text-success">Comment successfully created.</p>');
    $("#page_comments .media-list").append('<li class="media"><a class="pull-left" href="' + gon.user.profile + '"><img alt="Progile pic" class="sthumbnail" src="'+ gon.user.avatar_thumb +'"></a><div class="media-body"><p class="media-heading margin-0 small help-block">Commented By <strong><em>'+ gon.user.name +'</em></strong></p>'+  $('form.new_comment textarea').val() +'</div></li>')
    $('form.new_comment textarea').val("");
    $("#page_comments #table-scroll").animate({ scrollTop: $("#page_comments #table-scroll")[0].scrollHeight}, 200);
  });
  $('form.new_comment').on('ajax:error', function() {
    $('form.new_comment').prepend('<p class="bg-danger text-danger padding-10">OOps! There was an error. Please try again later.</p>');
    setTimeout(function(){
      $('form.new_comment .bg-danger').fadeOut().remove();
    }, 3000)
  });
}
function loadComments(page){  
  $.ajax({
    url: "/comments?page=" + page,
    success: function(data){
      $("body").append(data);
      $(".modal").modal("hide");
      // if(gon.action != "show" || $("#my-sqooz-show-page").hasClass("no-framecode")){
        
      // }
      $("#page_comments").modal("show");
      onCommentSubmitEvent();
      if (gon.action == "show"){
        $("#show-page-comment-bar").append($(data).find("#table-scroll"))
      }
    },
    error: function(){
      showFlashMessge("OOps! There was an error loading the comments.")
    }
  });
}

$(window).load(function(){
  if(gon.action == "show" && gon.controller == "pages"){
    hideSpinner();
    if(!window.sqoozPromtShown){
      // setTimeout(function(){

      //   showSqoozPrompt("Press the button to Sqooz the next video!")
      // },30000)
    }
  }
});

$(document).ready(function() {
  if(gon.action == "show" && gon.controller == "pages"){
    showSpinner();
    setTimeout(function(){
      hideSpinner();
    },6000)
  }

  // // Form validation starts here
  // // TODO: make validation work on edit page
  window.validator = new FormValidator($(".page-form").attr("id"), [{
    name: 'page[url]',
    rules: 'required|valid_url'
  }, {
      name: 'page[title]',
      rules: 'required'
  }, {
      name: 'page[interest_id]',
      rules: 'required'
  }, {
      name: 'page[channel_id]',
      rules: 'required'
  },{
    name: 'page[tag_list]',
    rules: 'required'
  }], function(errors, event) {
    $("#new_page .has-error").removeClass("has-error");
    if (errors.length > 0) {
     
      $.each(errors, function(index, error){
        $('[name="'+ error.name +'"]').parents(".form-group").addClass("has-error");
      });
      
    }
    else{
      return true
      // if($("#carousel-container").is(":visible")){
      //   $("#new_page").append('<input id="external_image" name="page[photo_remote_url]" type="hidden" value="'+ $(".carousel-inner .item.active img").attr("src") +'">')
      //   $("#new_page").append('<input id="embed_id" name="page[embed_id]" type="hidden" value="'+ parseForEmbedId($("#new_page input#page_url").val()) +'">')
      // }
    }
      
  });
  // // Form validation ends here

  // Rebort about the page
  $("#report-links a").on('ajax:success', function(event, xhr, status) {
    if($(this).hasClass("performed")){
      $(this).removeClass("performed");
      removeParamFromLink($(this), "reverse=true")
    }else{
      $(this).addClass("performed");
      addParamToLink($(this), "reverse=true")
    }
    showFlashMessge("You request was updated.")
  }).on("ajax:error", function(event, xhr, status){
    showFlashMessge("There was a problem. Please try again.")
  }).on("ajax:complete", function(event, xhr, status){

  });
  // Report about the page ends here

 
  $("#my-grid-container, #top-navigation, #show-page-share-bar, #my-list-container").on("click", ".comment-on-page" , function(){
    if(!$("#page_comments")[0]){
      loadComments($(this).data("page"));
    }else{
      $(".modal").modal("hide");
      if(gon.action == "show" && gon.controller == "pages"){
        $("#page_comments").modal("show");
      }else{
        $("#page_comments").remove();
        loadComments($(this).data("page"));
      }
    }
    return false;
  });
  // comment Modal ends

  // Share page modal open
  $("#my-grid-container, #top-navigation, #show-page-share-bar, #my-list-container").on('click',".share-the-page", function(){
    if(typeof addthis  === 'undefined'){
      // TODO: update urls for addthis here
      addthisreload(function(){$("#share-the-page").modal("show")})
    }else{
      $("#share-the-page").modal("show");
      var $this = $(this)
        addthis.update('share', 'url', $this.data("url"));
        addthis.update('share', 'title', $this.data("title"));
        addthis.update('share', 'description', $this.data("description"));
    }
    
    return false;
  });
  // Share page modal close

  // Add To List Modal
  function loadLists(page){
    $.ajax({
      url: "/lists?page=" + page,
      success: function(data){
        $("body").append(data);
        $(".modal").modal("hide");
        $("#add-to-list").modal("show");
        makeListTypeAhead();
      },
      error: function(){
        showFlashMessge("OOps! There was an error. Please try again later.")
      }
    });
  }

  $("#my-grid-container, #top-navigation, #show-page-share-bar, #my-list-container").on("click",'.add-to-list' , function(){
    if(!$("#add-to-list")[0]){
      loadLists($(this).data("page"));
    }else{
      $('.modal').modal('hide');
      $("#add-to-list #new_listing input#listing_page_id, #add-to-list #new_list #list_listings_attributes_0_page_id").val($(this).data("id"))
      $("#add-to-list #new_listing input#listing_interest_id, #add-to-list #new_list #list_listings_attributes_0_page_id").val($(this).data("interest_id"))
      $("#add-to-list").modal("show");
    }
    return false
  });
  // Add To List Modal

  // Make list typeahead
  var substringMatcher = function(lists) {
    return function findMatches(q, cb) {
      var matches, substringRegex;
      matches = [];

      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');

      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(lists, function(i, list) {
        if (substrRegex.test(list.title)) {
          // the typeahead jQuery plugin expects suggestions to a
          // JavaScript object, refer to typeahead docs for more info
          matches.push({ value: list.title, id:list.id });
        }
      });
      matches.push({ value: "+Create", id: null });
      cb(matches);
    };
  };

  var makeListTypeAhead = function(){
    $('#list-typeahead').typeahead({
      minLength: 3,
      autoselect: true,
      hint: true,
      highlight: true,
      minLength: 1
    }, 
    {
      name: 'listitems',
      displayKey: 'value',
      source: substringMatcher(gon.user.lists)
    }).on('typeahead:opened', function($e){
      var owidth = $($e.delegateTarget).outerWidth();
      $(".tt-dropdown-menu").width(owidth)
    }).on('typeahead:selected', function($e, datum, name) {
      if(datum.id){
        $("#add-to-list form#new_listing #listing_list_id").val(datum.id);
        $("#add-to-list form#new_listing").submit();
      }else{
        $("#add-to-list form#new_list").show();
        $("#add-to-list form#new_listing").hide();
      }
    });

    $("form#new_listing, form#new_list").on('ajax:success', function() {
        $("#add-to-list #choose-from-list form").hide();
        $("#add-to-list #choose-from-list").hide();
        $(".modal-header").hide();
        $(".modal-header.success").show();
        setTimeout(function(){
          $("#add-to-list").modal("hide");
          $("#page_comments").modal("hide");
          $("#add-to-list #choose-from-list").show();
          $("#add-to-list #choose-from-list form#new_listing").show();
          $(".modal-header").show();
          $(".modal-header.success").hide();
          $("#add-to-list #choose-from-list").show();
          $("form#new_listing")[0].reset();
          $("form#new_list")[0].reset();
          $("#add-to-list .has-error").removeClass("has-error");
        },3000)
    }).on('ajax:error', function(event, xhr, status) {
      var $form =$(event.target)
      if($form.hasClass("new_listing")){
        if(xhr.responseJSON.message.page_id){
          $("#add-to-list .modal-body").prepend('<div class="alert alert-success">The page is already in the list</div>');
          setTimeout(function(){
            $("#add-to-list .modal-body .alert").fadeOut("slow", function(){
              $(this).remove()
            });
          }, 3000);
        }
        else{
          showFlashMessge("There was an error. Please try again.")
        }
      }else if($form.hasClass("new_list")){
        $("#add-to-list .modal-body").prepend('<div class="alert text-center alert-success">There was an error. Please try again.</div>');
          setTimeout(function(){
            $("#add-to-list .modal-body .alert").fadeOut("slow", function(){
              $(this).remove();
            });
          }, 3000);
      }
    });
  }
  // Make list typeahed ends

  // Like or Unlike page

  $("a#unlike-page, li#unlike-page a").on("click", function(){
    likePage(like=false);
    return false;
  });

  $("a#like-page, li#like-page a").on("click", function(){
    likePage(like=true);
    return false;
  });
  
  // Like or Unlike page ends

  // Show page splash Content starts
    if(window.innerWidth >= 600){
      $('#my-sqooz-show-page #my-splash-box').css("min-height",$(window).height() * 0.7);
      //  $('#my-sqooz-show-page #splash-content').height($('#my-sqooz-show-page #my-splash-box').height())
       // $('#my-sqooz-show-page iframe').height($('#my-sqooz-show-page #my-splash-box').height()).width($('#my-sqooz-show-page #my-splash-box').width() * 0.5)

    }
    else{
      $('#my-sqooz-show-page #my-splash-box').css("min-height", $(window).height() * 0.8);
      // $('#my-sqooz-show-page  iframe').height($('#my-sqooz-show-page #my-splash-box').height()).width($('#my-sqooz-show-page #my-splash-box').width() )
    }

  if(gon.action == "show" && gon.controller == "pages"){
    showSpinner();
  }



  if(!$.cookie("no_best_video_overlay") && gon.best_video_overlay){
    // setTimeout(function(){
    //   $("body").on("mouseleave", function(e){
    //     showBestVideosOverlay();
    //   });
    // }, 1000*40)

  }


  $("#issue-with-playing").on("click", function(){
    $("div#go-to-site").modal("show");
    $("div#go-to-site .special-link").on("click", function(){
      $("div#go-to-site").modal("hide");
    });
  });
  
});
