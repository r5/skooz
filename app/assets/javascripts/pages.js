// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// Addthis social toolbox starts here
var addthis_config = {
  "data_track_addressbar":true 
}
var addthisreload = function(callback) {
  // Remove all global properties set by addthis, otherwise it won't reinitialize
  for (var i in window) {
    if (/^addthis/.test(i) || /^_at/.test(i)) {
      delete window[i];
    }
  }
  window.addthis_share = null;

  window.addthis_config = {
    data_track_clickback: false,
    data_track_addressbar: false
  } 

  // Finally, load addthis
  $.getScript("//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-532d2f76778dd435", function(){
    if(callback)
    callback()
  });
}

function ReinitializeAddThis(){
  if (window.addthis){

  window.addthis.ost = 0;
  window.addthis.ready();
  }
}
$(window).load(function(){
  if(!detectmob()){
    if(gon.controller == "pages"){
      if(!gon.load_social_immediate){
        setTimeout(function(){
          addthisreload();
        }, 6000)
      }
    }
    window.loaded_addthis = true
  }
});

// Addthis social toolbox ends here

function send_follow_unfollow_request(resourceType, resourceId, follow , $that){
  $.ajax({
    type: "post",
    url: "/follow",
    dataType: "json",
    data: {resource_type: resourceType, resource_id: resourceId, follow: follow },
    success: function(evt, xhr, status){
       if(follow == 1){
        
        if($that.hasClass("jlink")){
          $that.addClass("unfollow").removeClass("follow").text("Unfollow");
           $that.attr( "onclick", $that.attr("onclick").replace("follow", "unfollow"))
        }else{
          $that.attr("disabled", "disabled")
          $that.next().removeAttr("disabled")
        }
      }else{
        if($that.hasClass("jlink")){
          $that.addClass("follow").removeClass("unfollow").text("Follow")
          $that.attr( "onclick" ,$that.attr("onclick").replace("unfollow", "follow"))
        }else{
          
          $that.attr("disabled", "disabled")
          $that.prev().removeAttr("disabled")
        }
        
        
      }
    },
    error: function(){
      showFlashMessge("There was an error. Please try again.")
    }
  });
}

function follow(resourceType, resourceId, $this){
  send_follow_unfollow_request(resourceType, resourceId, 1, $this)
  return false;
}

function unfollow(resourceType, resourceId, $this){
  send_follow_unfollow_request(resourceType, resourceId,0,  $this)
  return false;
}
$(window).scroll(function(){
  if($(window).scrollTop() == $(document).height() - $(window).height()){
    if(gon.next_page){
      loadPages(gon.next_page);
      if(gon.page_count > gon.next_page){
        gon.next_page = gon.next_page + 1
      }else{
        gon.next_page = null
      }
    }
  }
}); 

function insertPage(data){
  if($("#masnory-container")[0]){
    $("#masnory-container").gridalicious('append', $(data));
  }else if($("#my-tag-index-container")[0]){
    $("#my-tag-index-container").append(data);
  }
  else{
    $("#my-grid-container ul:first, #my-list-container ul:first").append(data);
    // if($("#sidebar:visible")[0]){
      // if(window.innerWidth > 1100 && window.innerWidth < 1400){
      //   $("#my-list-container .thumb-container").addClass("col-lg-3").removeClass("col-lg-2");
      //   $("#my-list-container .media-body-container").addClass("col-lg-9").removeClass("col-lg-10");
      // }
    // }else{
      // if(window.innerWidth > 1100 && window.innerWidth < 1400){
      //   $("#my-list-container .thumb-container").removeClass("col-lg-3").addClass("col-lg-2");
      //   $("#my-list-container .media-body-container").removeClass("col-lg-9").addClass("col-lg-10");
      // }
    // }
  }
}
function loadPages(page){
  debugger
  $.ajax({
    dataType: "script",
    type: 'get',
    url: (gon.page_path + "/" + page.toString() + (gon.extra_params ? ("?" + gon.extra_params)  : "") ),
    success: function(data){
      insertPage(data)
    },
    error: function(data){
      insertPage(data.responseText) // Maa chudda TODO Fix this jquery issue where eror block is always called
    }
  })
} 
  

// Pagination ends here

$(document).ready(function() {

  if(window.loaded_addthis){
    addthisreload();
  }

  if(gon.load_social_immediate){
    addthisreload(function(){
      addthis.update('share', 'url', gon.page.slug_url);
      addthis.update('share', 'title', gon.page.title);
      addthis.update('share', 'description', gon.page.description);
    });
    window.loaded_addthis = true

  }

  $(".navbar-inverse #interest-links .dropdown-menu a").click(function(){
    var intId = $(this).data("id");
    $(".navbar-inverse #interest-links .dropdown-toggle").html($(this).text() + "<span class='caret'></span>")
    $("#search_form #int").val(intId)
    $("#sqooz-from #int").val(intId)
    // $.cookie("current_interest_id", intId)
    // $.cookie("current_interest_name", $(this).text());
  });
 

  // Get thubnails from video link
  // $("#custom_new_page_form #go-get-page").on("click",function(){
  //   var urlRegex = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
  //   var webadd = $("#custom_new_page_form #custom_url").val();
  //   if(webadd.length > 1 && urlRegex.test(webadd)){
  //     // Get thubnails
  //     $.ajax({
  //       type: "post",
  //       url: "/pages/getthumb",
  //       data: {webadd: webadd },
  //       beforeSend: function(){
  //         $("#custom_new_page_form .loading-img").show();
  //         $("#image-upload").addClass("my-hidden");
  //       },
  //       error: function(){
  //         showFlashMessge("There were some errors! Please try again.")
  //       },
  //       success: function(data){
  //         if(data.title && data.title.length > 1){
  //           $(".page-form #page_source_title").val(data.title);
  //           $(".page-form #page_source_title").find(".my-hidden").show();
  //         } 
  //         $(".page-form").removeClass("my-hidden")
  //         $("#custom_new_page_form").addClass("my-hidden");
  //         $(".page-form #page_url").val($("#custom_new_page_form #go-get-page").val());
  //         if(data.description && data.description.length > 1){
  //           $(".page-form #page_source_description").val(data.description)
  //           $(".page-form #page_source_description").find(".my-hidden").show();
  //         }
  //         if(data.images && data.images.length > 0){
  //           var str = ""
  //           $.each(data.images, function(ind, img){
  //             str = str + '<div class="item '+ (ind == 0 ? "active" : "") +'"><img src="'+ img.url +'"></div>'
  //           });
  //           $("#carousel-page-thumbs .carousel-inner").html(str);
  //           $("#carousel-container").removeClass("my-hidden");
  //           $("#image-upload").addClass("my-hidden");
  //           if(data.width_present){
  //             $("#carousel-page-thumbs").css("min-height", _.max(data.images, function(img){ return img.height/(data.images[0].width/300) }).height/(data.images[0].width/300))
  //           }
  //           else{
  //             $("#carousel-page-thumbs").css("min-height", 170)
  //           }
  //           if(data.images.length == 1){
  //             $(".carousel-control").hide();
  //           }else{
  //             $(".carousel-control").show();
  //           }
  //         }else{
  //           $("#image-upload").removeClass("my-hidden");
  //           $("#carousel-container").addClass("my-hidden");
  //         }
  //       },
  //       complete: function(){
  //         $("#new_page .loading-img").hide();
  //         $("#new_page input[type=submit]").removeAttr("disabled")
  //       }
  //     });
  //   }

  //   return false;
  // });

  // $("#new_page #upload-image").on("click", function(){
  //   $("#carousel-container").addClass("my-hidden");
  //   $("#image-upload").removeClass("my-hidden");
  //   return false;
  // });
  // Get thubnails from video link

  // list pages js starts here

  $("#my-grid-container").on("click", ".content-down", function(){
    var abt = $(this).parents(".page-container").find(".about-page");
    abt.show();
    abt.animate({top: 0}, 250)
  });

  $("#my-grid-container").on("click", ".content-up", function(){
    $(this).parents(".about-page").animate({top: (-$(this).parents(".page-container").height() -10)}, 250)
  });

  $("#masnory-container").gridalicious({selector: '.page-container', gutter: 20, width: 280});

  // var container = document.querySelector('#masnory-container');
  // var msnry = new Masonry( container, {
  //   itemSelector: '.page-container',
  //   columnWidth: 280
  // });
  // List pages js ends here
  
  
});
