// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//


//  Your other scripts should be loaded after jquery.turbolinks.js, and turbolinks.js should be after your other scripts.


// require medium-editor
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery.cookie
// require bootstrap/modal
// require bootstrap/dropdown
//= require bootstrap/affix
//= require bootstrap/alert
//= require bootstrap/transition
//= require nprogress
//= require nprogress-turbolinks
//= require nprogress-ajax
// require medium-editor-insert-plugin
// require medium-editor-insert-images
//= require froala_editor.min.js
//= require turbolinks
//= require about_page_medium
//= require classlist
//= require fitvids.js
function detectmob() {
   if(window.innerWidth <= 600) {
     return true;
   } else {
     return false;
   }
}

function showSpinner(text, time) {
  text = text || "Loading...";
  $("#loading-filter-background .loading-text").text(text);
  $("#loading-filter-background").fadeIn(time);
}
function hideSpinner(time) {
  $("#loading-filter-background").fadeOut(time);
}

function scriptLoader(src, callback) {

    var s = document.createElement('script');
    s.type = 'text/' + (src.type || 'javascript');
    s.src = src.src || src;
    s.async = false;

    s.onreadystatechange = s.onload = function () {

        var state = s.readyState;

        if (!callback.done && (!state || /loaded|complete/.test(state))) {
            callback.done = true;
            callback();
        }
    };

    // use body if available. more safe in IE
    (document.body || head).appendChild(s);
}

$(document).ready(function() {
    if(gon.controller == "pages" && (gon.action == "create" || gon.action == "new" || gon.action == "edit" || gon.action == "update")){
        scriptLoader("/assets/placeholder.js", function(){ $('input, textarea').placeholder() })
    }

    $("#my-sqooz-show-page").fitVids({ customSelector: "iframe.svideo"})
});
