function parseForEmbedId(url) {
  if(url.match(/youtu/)){
    return url.match(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/)[2]
  }else{
    return ""
  }
}