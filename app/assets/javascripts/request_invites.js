$(document).ready(function() {
  // // Form validation starts here
  // // TODO: make validation work on edit page
  window.req_validator = new FormValidator('new_request_invite', [{
    name: 'request_invite[name]',
    rules: 'required'
  }, {
      name: 'request_invite[email]',
      rules: 'required|valid_email'
  },{
    name: 'request_invite[about]',
    rules: 'required'
  },{
    name: 'request_invite[fb_link]',
    rules: 'required'
  },{
    name: 'request_invite[what_write]',
    rules: 'required'
  }], function(errors, event) {
    $("#new_request_invite .has-error").removeClass("has-error");
    if (errors.length > 0) {
     
      $.each(errors, function(index, error){
        $('[name="'+ error.name +'"]').parents(".form-group").addClass("has-error");
      });
      
    }
    else{
      $('#new_request_invite').submit();
      return true
     
    }
      
  });
  // // Form validation ends here
});