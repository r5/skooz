// Falsh message
function showFlashMessge(message){
  
  $("#top_wrapper").prepend('<div  class="margin-bottom-0 text-center alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ message +'</div>')
  $("#top_wrapper .alert").show().delay(800).addClass("in").fadeOut(4000, function(){
    $("#top_wrapper .alert").remove();
  });
}

function showFlashMessgePermanent(message){
$("#top_wrapper").prepend('<div  class="margin-bottom-0 alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ message +'</div>')
}
// Flasg message ends

// add a parameter to link jquery
function addParamToLink($ancor, paramstr){
  $ancor.attr('href', function(i, h) {
     return h + (h.indexOf('?') != -1 ? ("&" + paramstr) : ("?" + paramstr));
  });
}

function removeParamFromLink($ancor, paramstr){
  $ancor.attr('href', function(i, h) {
     return h.replace(("&" + paramstr), "").replace(("?" + paramstr), "")
  });
}
// add a parameter to link jquery ends

// Signin and Signup Modal JS STARTS //
$(document).on("click", ".login" ,  function(){
  $('.modal').modal('hide');
  $("#login_modal").modal("show");
  return false;
});
// Signin and Signup Modal JS ENDS //

$(document).ready(function() {
  $(".alert").delay(300).addClass("in").fadeOut(4000);  

  if(detectmob()){
    showFlashMessgePermanent("Sorry, We do not yet fully support mobile browsers! We are improving ourselves.")
  }

  // Sidebar js starts here
  $("#sidebar .collapse").on('show.bs.collapse', function () {
    $("#sidebar .collapse.in").collapse('hide');
    $("#sidebar .caret").addClass("right");
    $(this).parent().find(".caret").removeClass("right");
    $(".list-group-item.heading.active").removeClass("active");
    $(this).parent().find("a.heading").addClass("active");
  });
  $(".list-group-item.active").parents(".list-group").first().find(".collapse").collapse("show");

  $("#sidebar-toggle").on("click", function(){
    if($("#sidebar:visible")[0]){
      if(detectmob()){ // Show all elements that were hidden before
        $(".navbar-nav li").show();
        // $(".horiz-navbar li.first").show()
      }
      $(".container").width($(".container").data("old-width"));
      // Following is required to maintan responsive in case of lists pages
      // $(".container").css({left: 0, position: "static" });
      // if(window.innerWidth > 1100 && window.innerWidth < 1400){
      //   $("#my-list-container .thumb-container").removeClass("col-lg-3").addClass("col-lg-2");
      //   $("#my-list-container .media-body-container").removeClass("col-lg-9").addClass("col-lg-10");
      // }
      // A PART OF THE ABOVE IN PAGES.JS WHERE WE INSERT NEW PAGES
      $.cookie("sidebar_hidden", true)      
    }else{
      if(detectmob()){ // Hide all element in the navbar but the toggle switch
        $(".navbar-nav li").hide();
        $(".horiz-navbar li.first").show()
      }
      $(".container").data("old-width", $(".container").width())
      $(".container").width($(".container").width() - 250);
      // Following is required to maintan responsive in case of lists pages
      // $(".container").css({left: 250, position: "fixed" });
      // if(window.innerWidth > 1100 && window.innerWidth < 1400){
      //   $("#my-list-container .thumb-container").addClass("col-lg-3").removeClass("col-lg-2");
      //   $("#my-list-container .media-body-container").addClass("col-lg-9").removeClass("col-lg-10");
      // }
      $.cookie("sidebar_hidden", false)
    }
    $("#sidebar").toggle();
    $("#top_wrapper").toggleClass("with-sidebar");
    $("#sqoozer-head").css("left", ($(".navbar-static-top").width()/2 - $("#sqoozer-head").width()/2))
  });

  if(!detectmob() && $("#sidebar").hasClass("shownow") && (gon.controller != "welcome" && !gon.controller.match("devise"))){
    // $("#sidebar-toggle").trigger("click") # TODO Revert this on launch
  }

  $( window ).resize(function() { // Center Sqooz button
    $("#sqoozer-head").css("left", ($(".navbar-static-top").width()/2 - $("#sqoozer-head").width()/2))
  }).trigger("resize");

  
  
  // Sidebar js ends here

  // Homepage and video show page splash content ends

    $('#my-splash-box.full-height #splash-content').css('margin-top', ($(window).height()/2) - ($('#my-splash-box.full-height #splash-content').height()/2));
    
  // Homepage splash content ends
});