// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


$("document").ready(function () {
  // $("#add-interests").gridalicious({selector: '.minterest', gutter: 10, width: 200});
  $("#add-interests li").not(".active").find("a").append("<span class='glyphicon glyphicon-plus'></span>")
  $("#add-interests li.active a").append("<span class='glyphicon glyphicon-minus'></span>")

});

$(document).on("click", "a.remove-interest", function(){
  var $that = $(this)
  $.ajax({
    url: "/interests/" + $that.data("id"),
    type: "delete",
    dataType: "json",
    data: {interest_id: $that.data("id")},
    success: function(){
      $that.parents("li").removeClass("active");
      $that.removeClass("remove-interest").addClass("add-interest");
      $that.find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    },
    error: function(){
      showFlashMessge("There was an error! Please try again.")
    }
  });
  return false;
});

$(document).on("click", "a.add-interest", function(){
  var $that = $(this)
  $.ajax({
    url: "/interests/" ,
    type: "post",
    dataType: "json",
     data: {interest_id: $that.data("id")},
    success: function(){
      $that.parents("li").addClass("active");
      $that.removeClass("add-interest").addClass("remove-interest");
      $that.find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    },
    error: function(){
      showFlashMessge("There was an error! Please try again.")
    }
  });
  return false;

});