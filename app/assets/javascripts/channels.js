 

$(document).ready(function() {
  // // Form validation starts here
  // // TODO: make validation work on edit page
  window.channel_validator = new FormValidator('new_channel', [{
    name: 'channel[title]',
    rules: 'required'
  }, {
      name: 'channel[about]',
      rules: 'required'
  }], function(errors, event) {
    $("#new_channel .has-error").removeClass("has-error");
    if (errors.length > 0) {
     
      $.each(errors, function(index, error){
        $('[name="'+ error.name +'"]').parents(".form-group").addClass("has-error");
      });
      
    }
    else{
      $('#new_channel').submit();
    }
      
  });
  // // Form validation ends here
  if($("#masnory-container-channel").length > 0){
    $("#masnory-container-channel").gridalicious({selector: '.channel-container', gutter: 15, width: 375});

    $( window ).resize(function() {
      $("#masnory-container-channel").width($("#masnory-container-channel .galcolumn").length * $("#masnory-container-channel .galcolumn").innerWidth() + 30)
    });
    $( window ).trigger("resize");
  }

  
});
