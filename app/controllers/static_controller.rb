class StaticController < ApplicationController
  before_filter :authenticate_user!, :only => [:paid_sqoozers, :send_sample_email]

  def welcome
    if current_user && !params.has_key?(:nop)
      redirect, message = redirect_after_signin
      unless redirect
        page = User.sqoozitnow(current_user)
        if page
          redirect_to page.slug_path
        end
      else
        redirect_to redirect, :notice => message
      end
    end
  end

  def paid_sqoozers
    redirect_to root_path , :notice => "The page can not be found" unless current_user.paid
  end

  def send_sample_email
    not_found if current_user.email != "vipin.itm@gmail.com"
    if params[:send_to].present?
      params[:send_to].each do |send_to|
        RequestInvite.create(:name => send_to[:name].strip, :email => send_to[:email].strip).invite(true)
      end
      redirect_to root_path, :success => "Sent all the invitations!"
    end
  end

  def about
  end

  def coming_soon
  end

  def help
  end
private
  
  def redirect_after_signin
    if current_user.interestings.empty?
      return [discover_interests_path, nil]
    else
      likes_user_path(current_user)
    end
  end
end
