class InterestsController < ApplicationController

  before_filter :authenticate_user!, :except => [:show, :index, :suggest]

  def create
    interesting = Interesting.find_or_initialize_by(:user_id => current_user.id, :interest_id => params[:interest_id])
    if interesting.new_record? && interesting.save
      respond_to do |format|
        format.json { render :json => {:message => "ok"}, :status => 201 }
      end
    else
      respond_to do |format|
        format.js { render :json => {:message => "error"}, :status => 404 }
      end
    end
  end

  def suggest
    InterestSuggestion.create(:title => params[:title], :description => params[:description], :user_id => current_user.id )
    body = "Interest Title: #{params[:title]}<br /> Description: #{params[:description]}<br /> User Name: #{current_user.slug}"
    Notifier.notify_support("New Interest Suggested", body )
    render js: "$('#suggest_interest_modal #notice').show(); $('#suggest_interest_modal form').find('input[type=text], textarea').val('');", :status => 201
  end

  def index
     @interests =  Interest.active.where("pages_count > 0").order("name")
  end

  def show
    @interest = Interest.friendly.find(params[:id])
    not_found unless @interest
    @pages = Page.where(:interest_id => @interest.id).order("likes_count desc").page(params[:page] || 1).includes(:interest)
    pagination_gon_variable(@pages.total_pages)
    respond_to do |format|
      format.html {render "show.html.erb"}
      format.js { render "show_xhr.html.erb", :layout => false, :status => 200 }
    end
  end


  def destroy
    interesting = Interesting.where(:user_id => current_user.id, :interest_id => params[:id]).first
    if interesting && interesting.destroy
      respond_to do |format|
        format.json { render :json => {:message => "ok"}, :status => 201 }
      end
    else
      respond_to do |format|
        format.js { render :json => {:message => "error"}, :status => 404 }
      end
    end
  end
end
