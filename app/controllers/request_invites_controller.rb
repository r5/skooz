class RequestInvitesController < ApplicationController
  before_action :set_request_invite, only: [:show, :edit, :update, :destroy]

  # GET /request_invites
  # def index
  #   @request_invites = RequestInvite.all
  # end

  # GET /request_invites/1
  def show
  end

  # GET /request_invites/new
  def new
    @request_invite = RequestInvite.new
  end

  # GET /request_invites/1/edit
  def edit
    not_found
  end

  # POST /request_invites
  def create
    @request_invite = RequestInvite.new(request_invite_params)

    if @request_invite.save
      begin
        Notifier.invite_request(@request_invite).deliver
      rescue Exception => e
        logger.error "Could not send email: #{e.message}"
      end
      redirect_to root_path, notice: 'We have recieved your request. Thanks for being patient.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /request_invites/1
  def update
    not_found
    if @request_invite.update(request_invite_params)
      redirect_to @request_invite, notice: 'Request invite was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /request_invites/1
  def destroy
    @request_invite.destroy
    redirect_to request_invites_url, notice: 'Request invite was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request_invite
      @request_invite = RequestInvite.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def request_invite_params
      params.require(:request_invite).permit(:name, :about, :author, :what_write, :email, :paid, :fb_link, :twitter_link)
    end
end
