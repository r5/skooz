class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_gon_user, :set_gon_action_and_controller, :all_interests
  before_action do |controller|
    # basic_authenticate
    
    # set_my_location_from_ip unless session[:mxmind_tried]
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def all_interests
    @all_interests = Interest.where("pages_count > 0").order("name")
  end

  # def set_no_ad
  #   if params.has_key?(:nads)
  #     gon.no_ads = true
  #   end
  # end

  def pagination_gon_variable(total_pages)
    unless request.xhr?
      gon.page_count = total_pages
      cur_page = (params[:page] || 1).to_i
      gon.next_page = (gon.page_count > cur_page) ? cur_page + 1 : nil
      paramtr  = request.url.split("?")
      gon.page_path = paramtr[0].gsub(/\/$/, "")
      gon.extra_params = paramtr[1]
    end
  end

  # def set_my_location_from_ip
  #   if !cookies[:lat] && !cookies[:lan] && !session[:mxmind_tried]
  #     db = MaxMindDB.new(File.expand_path(Rails.root) + '/GeoLite2-City.mmdb')
  #     ret = db.lookup(Rails.env.production?  ? request.ip : '23.23.111.203');
  #     if ret
  #       cookies.permanent[:lat] = ret["location"]["latitude"]
  #       cookies.permanent[:lng] = ret["location"]["longitude"]
  #     end
  #     session[:mxmind_tried] =  true
  #   end
  # end

  def set_gon_user
    gon.user = {:name => current_user.username, :id => current_user.id, :avatar_thumb => current_user.avatar(:thumb), :profile => feed_path} if current_user
  end

  def set_gon_action_and_controller
    gon.action = params[:action]
    gon.controller = params[:controller]
  end

  def notifications_count
    @pending_notification_count = current_user.notifications.pending.count if current_user
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:username, :email, :slug, :avatar, :paid, :author, :birthday, :male, :invite_for_job]
    devise_parameter_sanitizer.for(:account_update) << [:username, :email, :slug, :avatar, :paid, :author, :birthday, :male, :invite_for_job]
    devise_parameter_sanitizer.for(:sign_in) << [:email]
    devise_parameter_sanitizer.for(:accept_invitation) << [:username, :emai, :paid, :author, :birthday, :male, :avatar, :invite_for_job]
    devise_parameter_sanitizer.for(:accept_invitation) do |u|
      u.permit(:username, :email, :password, :password_confirmation,
             :invitation_token, :paid, :author, :birthday, :male, :avatar, :invite_for_job)
    end
  end

  # def basic_authenticate
  #   if Rails.env == 'production'
  #     authenticate_or_request_with_http_basic "OfferSlot" do |username, password|
  #       username == "admin@sqooz.com" && password == "sqoozing"
  #     end
  #   end
  # end
end
