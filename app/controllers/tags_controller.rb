class TagsController < ApplicationController

  def show
    if params[:name]
      @tag = params[:name].gsub("-", " ")
      @pages = Page.tagged_with(params[:name].gsub("-", " ")).page(params[:page] || 1)
      if @pages.present?
        Page.eagerLoadFor(@pages, [:interest])
        pagination_gon_variable(@pages.total_pages)
        respond_to do |format|
          format.html {render "interests/show.html.erb"}
          format.js { render "interests/show_xhr.html.erb", :layout => false, :status => 200 }
        end
      else
        not_found
      end
    else
      not_found
    end
  end

  def index
    @tags=  Page.tag_counts.order("taggings.tags_count desc").page(params[:page] || 1).per(120)
    pagination_gon_variable(@tags.total_pages)
    respond_to do |format|
      format.html {render "index.html.erb"}
      format.js { render "index_xhr.html.erb", :layout => false, :status => 200 }
    end
 
  end
end
