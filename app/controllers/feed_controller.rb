class FeedController < ApplicationController

  before_filter :authenticate_user!, :only => [:recommended, :activity]

  before_filter :notifications_count

  def recommended
    @pages = current_user.recommended_pages(params)
    pagination_gon_variable(@pages.total_pages)
    respond_to do |format|
      format.html {render "recommended.html.erb"}
      format.js { render "recommended_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def activity
    @activities = current_user.feed_activity(params)
    pagination_gon_variable(@activities.total_pages)
    respond_to do |format|
      format.html {render "activity.html.erb"}
      format.js { render "activity_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def trending
    @pages = User.feed_trending(params)
    pagination_gon_variable(@pages.total_pages)
    respond_to do |format|
      format.html {render "trending.html.erb"}
      format.js { render "trending_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def lists
    @following_ids = current_user ? Follow.select("followable_id").where(:follower_id => current_user.id, :follower_type => "User", :blocked => false, :followable_type => 'List' ).map(&:followable_id) : []
    @lists = User.feed_trending_lists(params)
  end
end
