class PagesController < ApplicationController

  before_filter :authenticate_user!, :except => [:show, :sqoozme, :search]
  before_filter :find_page , :only => [:edit, :show, :update, :about, :medium_photo_upload, :share_page]
  protect_from_forgery :except => [:medium_photo_upload]

  before_filter :notifications_count, :only =>  ["show", "new"]

  def show
    gon.page = @page
    gon.show_page = true
    @liked = current_user.likes.where(:page_id => @page.id).first.try(:liked) if current_user
    # @best_pages = User.best_pages
    # @player_issue = @page.detect_issue
    # gon.best_video_overlay =  true
    @framecode = @page.framecode
    @comments = @page.comments.active.includes(:user) unless current_user
    PageView.create(:page_id => @page.id, :user_id => current_user.id, :score => @page.score) if current_user
  end

  def sqoozme
    set_interest_cookie
    @page = User.sqoozitnow(current_user, params)
    if @page
      respond_to do |format|
        format.html { redirect_to @page.slug_path}
        format.json { render :json => @page.slug_path }
      end
    else
      respond_to do |format|
        format.html { redirect_to root_path(:nop => ""), :notice => "Sorry, could not #{SITENAME} any page at this time. Something is wrong. Please try again."}
        format.json { render :json => {:not_found => true} }
      end
    end
  end

  def search
    per_page = 30
    set_interest_cookie
    @pages, @count = Page.search(params)
    pagination_gon_variable(@count/30) if @count > 30
    respond_to do |format|
      format.html {render "search.html.erb"}
      format.js { render "search_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def create
    @page = Page.new(page_params.merge(:score => Page::SCORES["channel"]))
    respond_to do |format|
      if @page.save
        if !@page.about(false).present?
          format.html { redirect_to about_page_path(@page) }
        else
          format.html { redirect_to @page.slug_path, notice: 'Page successfully created.' }
          format.json { render action: 'show', status: :created, location: @page.slug_path }
        end
      else
        @interests = Interest.select("id, name").active.all
        @channels = current_user.channels
        format.html { render action: 'new' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def about
    gon.page_slug = @page.slug
    render :layout => 'medium.html.erb'
  end

  def medium_photo_upload
    pho = @page.photos.build(:pattachment => params[:file])
    pho.save
    render :json => { link: pho.pattachment(:original) }
  end

  def medium_photo_delete
    pho = @page.photos.build(:pattachment => params[:file])
    pho.save
    render :text => "ok", :status => 200
  end

  def new
    @page = Page.new
    @channels = current_user.channels
    @interests = Interest.select("id, name").active.all
  end

  def edit
  end

  def update
    respond_to do |format| 
      if @page.update_attributes(page_params)        
        format.html { 
          if params[:only_about]
            redirect_to share_page_path(:id => @page.slug, :notice => "Page successfully created")
          else
            redirect_to @page.slug_path, notice: 'Page successfully updated.'
          end
        }
        format.json { render action: 'show', status: :created, location: @page.slug_path }
      else
        @interests = Interest.select("id, name").active.all
        @lists = current_user.lists
        format.html { render action: 'edit' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def share_page
    gon.load_social_immediate = true
    gon.page = {:slug_url => @page.slug_url , :title => @page.title, :description => (render_to_string :partial => 'layouts/truncate') }
  end

  # def getthumb
  #   begin
  #   raise "Url Format Not Valid" unless URI::regexp(%w(http https)).match params[:webadd]
  #   object = LinkThumbnailer.generate(params[:webadd])
  #   if(object.image)
  #     imgs = [{:url => object.image}]
  #     width_present = false
  #   else
  #     imgs = object.images.reject{|s| s.size[0] < 100 && s.size[1] < 100}
  #     imgs = imgs.map do |img|
  #       {:url => img.source_url, :width => img.size[0], :height => img.size[1]} if img.size[0] > 100 && img.size[1] > 100
  #     end.compact.sort{|img| img[:width]}.reverse
  #     width_present = true
  #   end

  #   render :json => {:title => object.title, :description => object.description, :images => imgs, :width_present => width_present}, :layout => false
    

  #   rescue Exception => e
  #     Rails.logger.error e.message
  #     Rails.logger.error e.backtrace
  #     render :json => {:title => nil, :description => nil, :images =>[], :width_present => false}, :layout => false
  #   end
  # end

private
  
  def page_params
    params.require(:page).permit!
  end

  def find_page
    @page = Page.friendly.find(params[:id])
    not_found unless @page
  end

  def set_interest_cookie
    if params[:int].present?
      interest = Interest.find_by_id(params[:int])
      if interest
        if !cookies[:current_interest_id].present? || cookies[:current_interest_id].to_i != params[:int].to_i
          cookies[:current_interest_id] = interest.id
          cookies[:current_interest_name] = interest.name
        end
      else
        cookies.detete(:current_interest)
      end
    end
  end


end
