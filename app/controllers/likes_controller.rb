  class LikesController < ApplicationController
  before_filter :authenticate_user!
  
  def index
  end

  def create
    # When one try unlikes something, try adding the page to block list
    @page = Page.friendly.find(params[:page])
    not_found unless @page
    like =  Like.find_or_initialize_by(:page_id => @page.id, :user_id => current_user.id);
    like_or_unlike = (params[:like] == "true" ? true : false);
    render :text => "ok", :status => 201 and return if like.liked == like_or_unlike
    like.liked = like_or_unlike
    like.interest_id, like.channel_id = @page.interest_id, @page.channel_id
    if like.save
      render :text => "ok", :status => 201
    else 
      render :text => "error", :status => 422
    end
  end

  def destroy
  end
private
  
  def like_params
    params.require(:like).permit!
  end
end
