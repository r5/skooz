class ListingsController < ApplicationController
  def create
    listing = current_user.listings.build(listing_params)
    respond_to do |format|
      if listing.save
        format.html { render :text => "ok", :status => 201 }
        format.js { render :json => {}, :status => 201}
      else
        format.html { render :text => "not_ok", :status => 422 }
        format.json { render :json => {:error => true, :message => (listing.errors.present? ? listing.errors : [])}, :status => 422}
      end
    end
  end

  def destroy
  end
private
  def listing_params
    params.require(:listing).permit!
  end
end
