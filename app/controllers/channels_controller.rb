class ChannelsController < ApplicationController

  before_filter :authenticate_user!

  def new
    @channel = current_user.channels.build
  end

  def edit
  end

  def show
   @channel = Channel.friendly.find(params[:id])
   if @channel
    @pages = @channel.pages.page(params[:page] || 1).includes(:interest)
    respond_to do |format|
      format.html {render "channel.html.erb"}
      format.js { render "channel_xhr.html.erb", :layout => false, :status => 200 }
    end
   else
    not_found
   end
  end

  def create
    @channel = current_user.channels.build(channel_params)
    respond_to do |format|
      if @channel.save
        format.html { redirect_to new_page_path, notice: 'Channle successfully created.' }
        format.json { render action: 'show', status: :created }
      else
        format.html { render action: 'new' }
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
  end
private
  def channel_params
    params.require(:channel).permit!
  end
end
