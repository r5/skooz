class DiscoverController < ApplicationController
  before_filter :authenticate_user!, :except => [:top_users, :top_channels]

  before_filter :notifications_count, :except => [:top_users, :top_channels]

  def interests
    @added_interest_ids, @interests =  current_user.interests_to_follow
  end

  def sqoozers
    @users = current_user.users_to_follow(params)
    @following_ids = Follow.select("followable_id").where(:follower_id => current_user.id, :follower_type => "User", :blocked => false, :followable_type => 'User' ).map(&:followable_id) if @users.present?
    pagination_gon_variable(@users.total_pages)
    respond_to do |format|
      format.html {render "sqoozers.html.erb"}
      format.js { render "sqoozers_xhr.html.erb", :layout => false, :status => 200 }
    end
  end


  def top_users
    @users = User.top_users(params)
    @following_ids = []
    pagination_gon_variable(@users.total_pages)
    respond_to do |format|
      format.html {render "sqoozers.html.erb"}
      format.js { render "sqoozers_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def top_channels
    @channels = User.feed_trending_channels(params)
    @following_ids = []
    pagination_gon_variable(@channels.total_pages)
    respond_to do |format|
      format.html {render "top_channels.html.erb"}
      format.js { render "top_channels.html.erb", :layout => false, :status => 200 }
    end
  end

  def lists
    @following_ids = []
    @lists = current_user.lists_to_follow(params)
    pagination_gon_variable(@lists.total_pages)
    respond_to do |format|
      format.html {render "lists.html.erb"}
      format.js { render "lists_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def channels
    @channels = current_user.channels_to_follow(params)
    pagination_gon_variable(@channels.total_pages)
    respond_to do |format|
      format.html {render "channels.html.erb"}
      format.js { render "channels_xhr.html.erb", :layout => false, :status => 200 }
    end
  end
  
end
