class ListsController < ApplicationController
  before_filter :authenticate_user!

  def create
    list = current_user.lists.build(list_params)
    respond_to do |format|
      if list.save
          format.html { render :text => "ok", :status => 201 }
          format.json { render :json => {}, :status => 201}
      else
        format.html { render :text => "not_ok", :status => 422 }
        format.json { render :json => {:error => true, :message => list.errors}, :status => 422}
      end
    end
  end

  def destroy
  end

  def index
    @page = Page.friendly.find(params[:page])
    not_found unless @page
    @lists = current_user.lists.select("id, title")
    if request.xhr?
      render "shared/_add_to_list_modal", :layout => false
    else
      render "shared/_add_to_list_modal"
    end
  end

private
  def list_params
    params.require(:list).permit!
  end
end
