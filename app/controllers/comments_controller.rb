class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def create
    comment = current_user.comments.build(comment_params)
    respond_to do |format|
      if comment.save
          format.html { redirect_to comment.page.slug_path, :notice => 'Comment was successfully created.' }
          format.js { render :json => {}, :status => 201}
      else
        format.html { redirect_to comment.page.slug_path, :notice => 'Comment could not be created.', :status => 422 }
        format.js { render :json => {:error => true}, :status => 422}
      end
    end
  end

  def destroy
  end

  def index
    @page = Page.friendly.find(params[:page])
    not_found unless @page
    @comments =  @page.comments.active.includes(:user)
    if request.xhr?
      render "shared/_comment_modal", :layout => false
    else
      render "shared/_comment_modal"
    end
  end

private
  
  def comment_params
    params.require(:comment).permit(:page_id, :user_id, :title, :content, :spam)
  end
end
