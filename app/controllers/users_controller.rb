class UsersController < ApplicationController

  before_filter :find_user, :except => [:report, :follow]
  before_filter :notifications_count, :except =>  ["follow", "report"]
  before_filter :authenticate_user!, :only => [:follow, :notifications]
  before_filter :recently_viewed, :only => [:pages, :likes, :following, :following_lists,:following_channels, :followers, :lists, :comments]

  # def pages  THIS IS OLD VERSION OD PAGE WHEN WE HAD LISTS. We may have to bring it back
  #   @interests = @user.all_listing_interests
  #   @lists = @user.lists.order("title")
  #   @listings = @user.filtered_pages(params)
  #   @list = List.friendly.find(params[:filter]) if params[:filter]
  #   @interest = Interest.friendly.find(params[:iname]) if params[:iname]
  #   pagination_gon_variable(@listings.total_pages)
  #   respond_to do |format|
  #     format.html {render "pages.html.erb"}
  #     format.js { render "pages_xhr.html.erb", :layout => false, :status => 200 }
  #   end
  # end

  def pages
    @interests = Interest.where(:id => @user.pages.select("distinct interest_id"))
    @channels = Channel.where(:id => @user.pages.select("distinct channel_id"))
    @channel = Channel.friendly.find(params[:cname]) if params[:cname]
    @interest = Interest.friendly.find(params[:iname]) if params[:iname]
    @ptype = Page::PTYPES[params[:ptype]] if params[:ptype]
    if @interest
      @pages = @user.pages.where(:interest_id => @interest.id).page(params[:page] || 1).order("paged_at desc");
    elsif @channel
      @pages = @user.pages.where(:channel_id => @channel.id).page(params[:page] || 1).order("paged_at desc");
    elsif @ptype
      @pages = @user.pages.where(:ptype => @ptype).page(params[:page] || 1).order("paged_at desc");
    else
      @pages = @user.pages.page(params[:page] || 1).order("paged_at desc");
    end
  end

  def likes
    # TODO check if more indexes can be used here
    @interests = @user.all_likes_interests
    @channels = @user.all_likes_channels
    @likes = @user.filtered_likes(params)
    pagination_gon_variable(@likes.total_pages) if @likes.present?
    @interest = Interest.friendly.find(params[:iname]) if params[:iname]
    @channel = Channel.friendly.find(params[:cname]) if params[:cname]
    respond_to do |format|
      format.html {render "likes.html.erb"}
      format.js { render "likes_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def follow
    resource = params[:resource_type].constantize.find(params[:resource_id])
    if(params[:follow] == "1")
      current_user.follow(resource)
    elsif(params[:follow] == "0")
      current_user.stop_following(resource)
    end
    respond_to do |format|
      format.html {render :text => "", :status => 201}
      format.json { render :json => {:message => "ok"}, :status => 201 }
    end
  end

  def following
    @users = @user.following_by_type("User").order("follows.created_at desc").page(params[:page] || 1)
    pagination_gon_variable(@users.total_pages)
    respond_to do |format|
      format.html {render "following.html.erb"}
      format.js { render "following_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def following_lists
    @lists = @user.following_by_type("List").order("follows.created_at desc").includes(:user).page(params[:page] || 1)
    pagination_gon_variable(@lists.total_pages)
    respond_to do |format|
      format.html {render "following_lists.html.erb"}
      format.js { render "following_lists_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def following_channels
    @channels = @user.following_by_type("Channel").order("follows.created_at desc").page(params[:page] || 1).includes(:user)
    pagination_gon_variable(@channels.total_pages)
    respond_to do |format|
      format.html {render "following_channels.html.erb"}
      format.js { render "following_channels_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def followers
    @users = @user.user_followers.page(params[:page] || 1)
    @following_ids = Follow.select("followable_id").where(:follower_id => @user.id, :follower_type => "User", :blocked => false, :followable_type => 'User' ).map(&:followable_id) if @users.present?
    pagination_gon_variable(@users.total_pages)
    respond_to do |format|
      format.html {render "followers.html.erb"}
      format.js { render "followers_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def interests
    @interests = @user.interests.page(params[:page] || 1)
    pagination_gon_variable(@interests.total_pages)
    respond_to do |format|
      format.html {render "interests.html.erb"}
      format.js { render "interests_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def history
    @interests = @user.interests.select("name")
    @pageviews = @user.page_views.order("viewed_at desc").page(params[:page] || 1).includes(:page)
    pagination_gon_variable(@pageviews.total_pages)
    respond_to do |format|
      format.html {render "history.html.erb"}
      format.js { render "history_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def lists
    @lists = @user.lists.page(params[:page] || 1).order("updated_at desc")
    pagination_gon_variable(@lists.total_pages)
    respond_to do |format|
      format.html {render "lists.html.erb"}
      format.js { render "lists_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def notifications
    @following_ids = Follow.select("followable_id").where(:follower_id => @user.id, :follower_type => "User", :blocked => false, :followable_type => 'User' ).map(&:followable_id)
    @notifications = @user.notifications.latest.includes(:page, :actor).page(params[:page] || 1).per(40)
    pagination_gon_variable(@notifications.total_pages)
    @notifications.update_all(:viewed => true) # Run this in background
    respond_to do |format|
      format.html {render "notifications.html.erb"}
      format.js { render "notifications_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def comments
    @comments = @user.comments.page(params[:page] || 1).order("created_at desc").includes(:page)
    pagination_gon_variable(@comments.total_pages)
    respond_to do |format|
      format.html {render "comments.html.erb"}
      format.js { render "comments_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def tags
    @tags = @user.pages.tag_counts_on(:tags)
  end


  def channels
    @channels = @user.channels.page(params[:page] || 1).includes(:user)
    @following_ids = Follow.select("followable_id").where(:follower_id => @user.id, :follower_type => "User", :blocked => false, :followable_type => 'Channel' ).map(&:followable_id) if current_user != @user
    pagination_gon_variable(@channels.total_pages)
    respond_to do |format|
      format.html {render "channels.html.erb"}
      format.js { render "channels_xhr.html.erb", :layout => false, :status => 200 }
    end
  end

  def show
    redirect_to likes_user_path(@user), :status => :moved_permanently 
  end

  def report
    page = Page.friendly.find(params[:pageid])
    no_reverse = !params[:reverse]
    if page
      if params[:notme]
        no_reverse ? current_user.not_for_me(page) : current_user.yes_for_me(page)
      elsif params[:seenit]
        no_reverse ? current_user.seen_it_already(page) : current_user.unseen_it_already(page) 
      elsif params[:pagenoload]
        no_reverse ? current_user.report_page_no_load(page) : current_user.page_does_load(page)
      elsif params[:spam]
        no_reverse ? current_user.report_spam(page) : current_user.report_unspam(page)
      elsif params[:block]
        no_reverse ? current_user.block_this_page(page) : current_user.unblock_this_page(page)
      end
      next_page = current_user.sqoozit unless request.xhr?
      respond_to do |format|
        format.html { redirect_to next_page.slug_path, :notice => "Your request was updated.", :status => 200}
        format.json {render :json => {:message => "ok" }.to_json, :status => 200}
      end
    else
      respond_to do |format|
        format.html { not_found}
        format.json { render :json => {:error => "not_found"}, :status => 422 }
      end
    end
  end

private
  def find_user
    @user = User.friendly.find(params[:id])
    not_found unless @user
  end

  def recently_viewed
    @recently_viewed = @user.page_views.select("distinct page_id").limit(5).order("viewed_at desc").includes(:page)
  end
end
